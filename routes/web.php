<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'overhaul','prefix' => 'overhaul'],function(){
    Route::get('dashboard','Overhaul\DashboardController@index');
    Route::get('device.repair','Overhaul\DeviceRepairController@index');
    Route::patch('device.repair/{id}','Overhaul\DeviceRepairController@update');
    Route::get('device.repair/{id}','Overhaul\DeviceRepairController@get_where');
    //logout
    Route::get('logout','Overhaul\SigninController@logout');
});
Route::group(['middleware' => 'auth','prefix' => 'admin'],function(){
    Route::get('dashboard','Admin\DashboardController@index');
    Route::get('webinfo','Admin\WebinfoController@index');
    Route::post('webinfo','Admin\WebinfoController@update');
    Route::get('admin','Admin\AdminController@index');
    Route::get('admin/{id}','Admin\AdminController@get_data');
    Route::post('admin','Admin\AdminController@create');
    Route::patch('admin/{id}','Admin\AdminController@update');
    Route::patch('admin/change/{id}','Admin\AdminController@change_password');
    Route::delete('admin/{id}','Admin\AdminController@delete');
    // Overhaul route
    Route::get('overhaul','Admin\OverhaulController@index');
    Route::get('overhaul/{id}','Admin\OverhaulController@get_data');
    Route::post('overhaul','Admin\OverhaulController@create');
    Route::patch('overhaul/{id}','Admin\OverhaulController@update');
    Route::patch('overhaul/change/{id}','Admin\OverhaulController@change_password');
    Route::delete('overhaul/{id}','Admin\OverhaulController@delete');
    //學年度資料
    Route::get('school.year','Admin\SchoolyearController@index');
    Route::get('school.year/{id}','Admin\SchoolyearController@get_data');
    Route::post('school.year','Admin\SchoolyearController@create');
    Route::patch('school.year/{id}','Admin\SchoolyearController@update');
    Route::delete('school.year/{id}','Admin\SchoolyearController@delete');
    //設備分類
    Route::get('device.category','Admin\DevicecategoryController@index');
    Route::get('device.category/{id}','Admin\DevicecategoryController@get_data');
    Route::post('device.category','Admin\DevicecategoryController@create');
    Route::patch('device.category/{id}','Admin\DevicecategoryController@update');
    Route::delete('device.category/{id}','Admin\DevicecategoryController@delete');
    //設備名稱
    Route::get('device.name','Admin\DevicenameContoller@index');
    Route::get('device.name/{id}','Admin\DevicenameContoller@get_where');
    Route::post('device.name','Admin\DevicenameContoller@create');
    Route::patch('device.name/{id}','Admin\DevicenameContoller@update');
    Route::delete('device.name/{id}','Admin\DevicenameContoller@delete');
    //故障分類
    Route::get('fault.category','Admin\FaultCategoryController@index');
    Route::get('fault.category/{id}','Admin\FaultCategoryController@get_where');
    Route::post('fault.category','Admin\FaultCategoryController@create');
    Route::patch('fault.category/{id}','Admin\FaultCategoryController@update');
    Route::delete('fault.category/{id}','Admin\FaultCategoryController@delete');
    //狀態
    Route::get('repair.status','Admin\RepairStatusController@index');
    Route::get('repair.status/{id}','Admin\RepairStatusController@get_where');
    Route::post('repair.status','Admin\RepairStatusController@create');
    Route::patch('repair.status/{id}','Admin\RepairStatusController@update');
    Route::delete('repair.status/{id}','Admin\RepairStatusController@delete');
    //維修資料
    Route::get('device.repair','Admin\DeviceRepairController@index');
    Route::get('device.repair/{id}','Admin\DeviceRepairController@get_where');
    Route::patch('device.repair/{id}','Admin\DeviceRepairController@get_where');
    Route::delete('device.repair/{id}','Admin\DeviceRepairController@delete');
    //QrCode
    Route::get('qrcode','Admin\QrcodeController@index');
    Route::get('qrcode/{id}','Admin\QrcodeController@get_where');
    Route::post('qrcode','Admin\QrcodeController@create');
    Route::patch('qrcode/{id}','Admin\QrcodeController@update');
    Route::delete('qrcode/{id}','Admin\QrcodeController@delete');
    Route::get('document/qrcode','Admin\QrcodeController@output_word');
    Route::get('document/muitl/qrcode','Admin\QrcodeController@muitl_output_word');

    Route::group(['prefix' => 'module'],function(){
        Route::get('hwai/drivelist','Admin\module\hwaiDriveListModuleController@index');
        Route::get('hwai/drivelist/create','Admin\module\hwaiDriveListModuleController@create');
        Route::post('hwai/drivelist','Admin\module\hwaiDriveListModuleController@store');
        Route::get('hwai/drivelist/{id}/update','Admin\module\hwaiDriveListModuleController@edit');
        Route::patch('hwai/drivelist/{id}','Admin\module\hwaiDriveListModuleController@update');
        Route::delete('hwai/drivelist/{id}','Admin\module\hwaiDriveListModuleController@destroy');
        Route::post('hwai/drivelist/import','Admin\module\hwaiDriveListModuleController@import');
        Route::get('hwai/drivelist/export','Admin\module\hwaiDriveListModuleController@export');
        Route::get('hwai/drivelist/sample','Admin\module\hwaiDriveListModuleController@sample');
        Route::get('hwai/drivelist/category','Admin\module\hwaiDriveListModuleController@document_category');
    });
    Route::get('logout','Admin\SignController@logout');
});
Route::group(['prefix' => 'auth','middleware' => 'guest'],function(){
    Route::get('admin','Admin\SignController@index');
    Route::post('admin','Admin\SignController@login');
    Route::get('overhaul','Overhaul\SigninController@index');
    Route::post('overhaul','Overhaul\SigninController@login');
});
// global ajax 通用AJAX資料請求URI
Route::group(['prefix' => 'ajax'],function(){
    Route::get('fault.category/{id}','Ajax\FaultCategoryController@get_option');
    Route::get('device.name/{id}','Ajax\DeviceNameController@get_option');
});
Route::get('/','RepairController@index');
Route::get('qrcode','RepairController@index');
Route::get('repair/list','RepairController@search');
Route::post('repair','RepairController@sendrepair');


