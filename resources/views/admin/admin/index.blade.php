@extends('coreui.admintemplate')

@section('title',$webdata->title." - 管理者帳戶資料管理")

@section('content')
    <!-- Jquery Validate -->
    <script type="text/javascript" src="{{ URL::asset('js/jquery/validate/jquery.validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery/validate/localization/messages_zh_TW.min.js') }}"></script>
    <!-- Vue js -->
    <script type="text/javascript" src="{{ URL::asset('js/vue/vue.min.js') }}"></script>
    <!-- DataTable Plugin -->
    <link rel="stylesheet" href="{{ URL::asset('css/bs4.datatable/dataTables.bootstrap4.min.css') }}">
    <script type="text/javascript" src="{{ URL::asset('js/bs4.datatable/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/bs4.datatable/dataTables.bootstrap4.min.js') }}"></script>
    <!-- modal create -->
    <div class="modal" tabindex="-1" role="dialog" id="modal_create">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">新增管理者</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="createFrom">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="username">帳戶名稱</label>
                            <input type="text" name="username" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="password">帳戶密碼</label>
                            <input type="password" name="password" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="name">登入名稱</label>
                            <input type="text" name="name" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- modal update -->
    <div class="modal" tabindex="-1" role="dialog" id="modal_update">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">更新管理者資料</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="updateForm">
                    <div class="modal-body">
                        {{ method_field('patch') }}
                        <div class="form-group">
                            <label for="username">帳戶名稱</label>
                            <input type="text" name="username" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="name">登入名稱</label>
                            <input type="text" name="name" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- model change password -->
    <div class="modal" tabindex="-1" role="dialog" id="modal_change">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">更改管理者密碼</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="changeForm">
                    <div class="modal-body">
                        {{ method_field('patch') }}
                        <div class="form-group">
                            <label for="password">新密碼</label>
                            <input type="password" name="password" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- model delete -->
    <div class="modal" tabindex="-1" role="dialog" id="modal_delete">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">刪除管理者</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="deleteForm">
                    <div class="modal-body">
                        <form id="deleteForm">
                            {{ method_field('delete') }}
                            <h4>確認是否要刪除以下資料，以下操作將不可逆</h4>
                            <div id="deleteView">
                                <table style="width: 100%">
                                    <tr>
                                        <td>使用者名稱</td>
                                        <td>@{{ username }}</td>
                                    </tr>
                                    <tr>
                                        <td>名稱</td>
                                        <td>@{{ name }}</td>
                                    </tr>
                                </table>
                            </div>
                        </form>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" onclick="action_delete()">刪除管理者</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div style="padding: 20px;">
        <div id="status-bar">
            <div class="alert alert-success" role="alert" v-show="isSuccess">
                操作成功！
            </div>
            <div class="alert alert-danger" role="alert" v-show="isFailed">
                操作失敗！
            </div>
        </div>
        <button type="button" class="btn btn-default" onclick="modal_create()">新增管理員</button><p>
        <table class="table" id="adminTable">
            <thead class="thead-dark">
                <th style="text-align: center;font-weight:bold;">管理者帳戶</th>
                <th style="text-align: center;font-weight:bold;">管理者名稱</th>
                <th style="text-align: center;font-weight:bold;">操作</th>
            </thead>
            <tbody>
            @foreach($result as $row)
                <tr>
                    <td style="text-align: center;">{{ $row->username }}</td>
                    <td style="text-align: center;">{{ $row->name }}</td>
                    <td style="text-align: center;">
                        <button type="button" class="btn btn-primary" onclick="modal_update({{ $row->id }})">更新資料</button>
                        <button type="button" class="btn btn-default" onclick="modal_change({{ $row->id }})">密碼更改</button>
                        @if($row->id != 1)
                        <button type="button" class="btn btn-danger" onclick="modal_delete({{ $row->id }})">刪除</button>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <script type="text/javascript">
        $('#adminTable').DataTable();
        var data_id = null;
        var status_bar = new Vue({
            el: '#status-bar',
            data:{
                isSuccess: false,
                isFailed: false
            }
        })
        var deleteView = new Vue({
            el: '#deleteView',
            data: {
                username: null,
                name: null
            }
        })
        $('#createFrom').validate({
            submitHandler: function(){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: '{{ url('admin/admin') }}',
                    method: 'post',
                    data: $('#createFrom').serialize(),
                    success: function(){
                        setTimeout(function(){
                            location.reload();
                        },2000);
                        Vue.set(status_bar,'isSuccess',true);
                        Vue.set(status_bar,'isFailed',false);
                        $('#modal_create').modal('hide');
                    },
                    error: function(){
                        Vue.set(status_bar,'isSuccess',false);
                        Vue.set(status_bar,'isFailed',true);
                        $('#modal_create').modal('hide');
                    }
                })
            },
            rules: {
                username: {
                    required: true,
                    minlength: 5,
                    maxlength: 20
                },
                password: {
                    required: true,
                    minlength: 5,
                    maxlength: 20
                },
                name: {
                    required: true,
                    minlength: 2,
                    maxlength: 10
                }
            }
        })
        $('#updateForm').validate({
            submitHandler: function(){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: '{{ url('admin/admin') }}/'+data_id,
                    method: 'post',
                    data: $('#updateForm').serialize(),
                    success: function(){
                        setTimeout(function(){
                            location.reload();
                        },2000);
                        Vue.set(status_bar,'isSuccess',true);
                        Vue.set(status_bar,'isFailed',false);
                        $('#modal_update').modal('hide');
                    },
                    error: function(){
                        Vue.set(status_bar,'isSuccess',false);
                        Vue.set(status_bar,'isFailed',true);
                        $('#modal_update').modal('hide');
                    }
                })
            },
            rules:{
                username: {
                    required: true,
                    minlength: 5,
                    maxlength: 20
                },
                name: {
                    required: true,
                    minlength: 2,
                    maxlength: 10
                }
            }
        })
        $('#changeForm').validate({
            submitHandler: function () {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: '{{ url('admin/admin/change') }}/'+data_id,
                    method: 'post',
                    data: $('#changeForm').serialize(),
                    success: function(){
                        setTimeout(function(){
                            location.reload();
                        },2000);
                        Vue.set(status_bar,'isSuccess',true);
                        Vue.set(status_bar,'isFailed',false);
                        $('#modal_change').modal('hide');
                    },
                    error: function(){
                        Vue.set(status_bar,'isSuccess',false);
                        Vue.set(status_bar,'isFailed',true);
                        $('#modal_change').modal('hide');
                    }
                })
            },
            rules: {
                password: {
                    required: true,
                    minlength: 5,
                    maxlength: 20
                }
            }
        });
        function action_delete(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '{{ url('admin/admin') }}/'+data_id,
                method: 'post',
                data: $('#deleteForm').serialize(),
                success: function(){
                    setTimeout(function(){
                        location.reload();
                    },2000);
                    Vue.set(status_bar,'isSuccess',true);
                    Vue.set(status_bar,'isFailed',false);
                    $('#modal_delete').modal('hide');
                },
                error: function(){
                    Vue.set(status_bar,'isSuccess',false);
                    Vue.set(status_bar,'isFailed',true);
                    $('#modal_delete').modal('hide');
                }
            })
        }
        function get_data(id){
            $.ajax({
                url: '{{ url('admin/admin') }}/'+id,
                method: 'get',
                success: function(data){
                    Vue.set(deleteView,'username',data['username']);
                    Vue.set(deleteView,'name',data['name']);
                    $("input[name='username']").val(data['username']);
                    $("input[name='name']").val(data['name']);
                }
            })
        }
        function modal_create(){
            $('#modal_create').modal('show');
        }
        function modal_update(id){
            if(typeof id === 'number'){
                data_id = id;
            }
            get_data(id)
            $('#modal_update').modal('show');
        }
        function modal_delete(id){
            if(typeof id === 'number'){
                data_id = id;
            }
            get_data(id)
            $('#modal_delete').modal('show');
        }
        function modal_change(id){
            if(typeof id === 'number'){
                data_id = id;
            }
            $('#modal_change').modal('show');
        }
    </script>
@endsection