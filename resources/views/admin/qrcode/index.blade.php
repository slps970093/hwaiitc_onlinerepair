@extends('coreui.admintemplate')

@section('title',$webdata->title." - QrCode行動報修")

@section('content')
    <!-- Jquery Validate -->
    <script type="text/javascript" src="{{ URL::asset('js/jquery/validate/jquery.validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery/validate/localization/messages_zh_TW.min.js') }}"></script>
    <!-- Vue js -->
    <script type="text/javascript" src="{{ URL::asset('js/vue/vue.min.js') }}"></script>
    <!-- DataTable Plugin -->
    <link rel="stylesheet" href="{{ URL::asset('css/bs4.datatable/dataTables.bootstrap4.min.css') }}">
    <script type="text/javascript" src="{{ URL::asset('js/bs4.datatable/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/bs4.datatable/dataTables.bootstrap4.min.js') }}"></script>
    <!-- Modal create -->
    <div class="modal" tabindex="-1" role="dialog" id="modalCreate">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">新增 Qr-Code 行動條碼報修</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="createForm">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="device_category">設備分類</label>
                            <select name="device_category" onchange="loading_device_name()" class="form-control">
                                <option selected="selected">please select</option>
                                @foreach($category as $row)
                                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="device_name">設備分類</label>
                            <select name="device_name" class="form-control">

                            </select>
                        </div>
                        <div class="form-group">
                            <label for="location">地點</label>
                            <input type="text" name="location" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">送出</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Modal Update -->
    <div class="modal" tabindex="-1" role="dialog" id="modalUpdate">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">更新 Qr-Code 行動條碼報修</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="updateForm">
                    {{ method_field('patch') }}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="device_category">設備分類</label>
                            <select name="device_category" onchange="loading_device_name()" class="form-control">
                                <option selected></option>
                                @foreach($category as $row)
                                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="device_name">設備分類</label>
                            <select name="device_name" class="form-control">

                            </select>
                        </div>
                        <div class="form-group">
                            <label for="location">地點</label>
                            <input type="text" name="location" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Modal Delete -->
    <div class="modal" tabindex="-1" role="dialog" id="modalDelete">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">刪除 Qr-Code 行動條碼報修</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="deleteForm">
                        <h4>請確認是否要刪除QR-CODE行動報修資料</h4>
                        {{ method_field('delete') }}
                        <table style="width: 100%;" id="dataView">
                            <tr>
                                <td>設備分類</td>
                                <td>@{{ category }}</td>
                            </tr>
                            <tr>
                                <td>設備名稱</td>
                                <td>@{{ name }}</td>
                            </tr>
                            <tr>
                                <td>地點</td>
                                <td>@{{ location }}</td>
                            </tr>
                        </table>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" onclick="actionDelete()">刪除資料</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div style="padding: 20px;">
        <div id="status-bar">
            <div class="alert alert-success" role="alert" v-show="isSuccess">
                操作成功！
            </div>
            <div class="alert alert-danger" role="alert" v-show="isFailed">
                操作失敗！
            </div>
        </div>

        <div class="btn-group" role="group" aria-label="Basic example">
            <button type="button" onclick="modal_create()" class="btn btn-default">新增</button>
            <button type="button" onclick="createWord()" class="btn btn-secondary">產生WORD文件（全部）</button>
            <button type="button" onclick="createMuitlWord()" class="btn btn-secondary">產生WORD文件（依照選取資料）</button>
        </div>
        <table class="table">
            <thead class="thead-dark">
                <th style="text-align: center;font-weight:bold;">選取</th>
                <th style="text-align: center;font-weight:bold;">設備分類</th>
                <th style="text-align: center;font-weight:bold;">設備名稱</th>
                <th style="text-align: center;font-weight:bold;">地點</th>
                <th style="text-align: center;font-weight:bold;">操作</th>
            </thead>
            <tbody>
                @foreach($result as $row)
                <tr>
                    <td style="text-align: center;"><input type="checkbox" name="id[]" class="form-control" value="{{ $row->id }}"></td>
                    <td style="text-align: center;">{{ $row->device_category->name }}</td>
                    <td style="text-align: center;">{{ $row->device_name->name }}</td>
                    <td style="text-align: center;">{{ $row->location }}</td>
                    <td style="text-align: center;">
                        <button type="button" class="btn btn-primary" onclick="modal_update({{ $row->id }})">更新</button>
                        <button type="button" class="btn btn-danger" onclick="modal_delete({{ $row->id }})">刪除</button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <script type="text/javascript">
        var data_id = null;
        var status_bar = new Vue({
            el: '#status-bar',
            data:{
                isSuccess: false,
                isFailed: false
            }
        })
        var dataView = new Vue({
            el: '#dataView',
            data:{
                category: null,
                name: null,
                location: null
            }
        })
        function createWord(){
            window.open('{{ url('admin/document/qrcode') }}');
        }
        function createMuitlWord(){
            window.open('{{ url('admin/document/muitl/qrcode') }}?'+$('input[name="id[]"]').serialize());
        }
        function modal_create(){
            $('#modalCreate').modal('show');
        }
        function modal_update(id){
            data_id = id;
            $('#modalUpdate').modal('show');
            get_data();
        }
        function modal_delete(id){
            data_id = id;
            $('#modalDelete').modal('show');
            get_data();
        }
        $('#createForm').validate({
            submitHandler: function(){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: '{{ url('admin/qrcode') }}',
                    method: 'post',
                    data: $('#createForm').serialize(),
                    success: function(){
                        Vue.set(status_bar,'isSuccess',true);
                        Vue.set(status_bar,'isFailed',false);
                        $('#modalCreate').modal('hide');
                        setTimeout(function(){
                            location.reload()
                        },5000);
                    },
                    error: function(){
                        Vue.set(status_bar,'isSuccess',false);
                        Vue.set(status_bar,'isFailed',true);
                        $('#modalCreate').modal('hide');
                    }
                })
            },
            rules: {
                device_name: 'required',
                device_category: 'required',
                location: {
                    required : true,
                    minlength: 2,
                    maxlength: 10
                }
            }
        })
        $('#updateForm').validate({
            submitHandler: function(){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: '{{ url('admin/qrcode') }}/'+data_id,
                    data: $('#updateForm').serialize(),
                    method: 'post',
                    success: function(){
                        Vue.set(status_bar,'isSuccess',true);
                        Vue.set(status_bar,'isFailed',false);
                        $('#modalUpdate').modal('hide');
                        setTimeout(function(){
                            location.reload()
                        },5000);
                    },
                    error: function(){
                        Vue.set(status_bar,'isSuccess',false);
                        Vue.set(status_bar,'isFailed',true);
                        $('#modalUpdate').modal('hide');
                    }
                })
            },
            rules: {
                device_name: 'required',
                device_category: 'required',
                location: {
                    required : true,
                    minlength: 2,
                    maxlength: 10
                }
            }
        });

        function loading_device_name(){
            var id = $('select[name="device_category"]').val();
            $.ajax({
                url: '{{ url('ajax/device.name') }}/'+$('select[name="device_category"]').val(),
                method: 'get',
                success: function(data){
                    $.each(data,function (index,value) {
                        $('select[name="device_name"]').append(new Option(value['name'],value['id']));
                    })
                },error: function(){
                    alert("載入資料發生錯誤，請聯絡系統管理員");
                }
            })
        }
        function update_nameSelect(id,defaultTarget){
            $.ajax({
                url: '{{ url('ajax/device.name') }}/'+id,
                method: 'get',
                success: function(data){
                    $('select[name="device_name"]').empty();
                    $.each(data,function (index,value) {
                        $('select[name="device_name"]').append(new Option(value['name'],value['id']));
                    })
                    $('select[name="device_name"]').val(defaultTarget);
                }
            })
        }
        function get_data(){
            $.ajax({
                url: '{{ url('admin/qrcode/') }}/'+data_id,
                method: 'get',
                success: function(data){
                    $('select[name="device_category"]').val(data['device_category_id']);
                    update_nameSelect(data['device_category_id'],data['device_name_id']);
                    $('input[name="location"]').val(data['location']);
                    Vue.set(dataView,'category',data['joinData']['device_category']);
                    Vue.set(dataView,'name',data['joinData']['device_name']);
                    Vue.set(dataView,'location',data['location']);
                },
                error: function(){
                    alert("資料載入發生錯誤，請重新整理再試一次，多次失敗請聯絡系統管理員");
                }
            })
        }
        function actionDelete(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '{{ url('admin/qrcode') }}/'+data_id,
                method: 'post',
                data: $('#deleteForm').serialize(),
                success: function(){
                    $('#modalDelete').modal('hide');
                    Vue.set(status_bar,'isSuccess',true);
                    Vue.set(status_bar,'isFailed',false);
                    setTimeout(function(){
                        location.reload()
                    },5000);
                },error: function(){
                    $('#modalDelete').modal('hide')
                    Vue.set(status_bar,'isSuccess',false);
                    Vue.set(status_bar,'isFailed',true);
                }
            })
        }
    </script>
@endsection