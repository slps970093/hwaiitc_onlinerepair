@extends('coreui.admintemplate')

@section('title',$webdata->title.'- 維修狀態管理')

@section('content')
    <!-- Jquery Validate -->
    <script type="text/javascript" src="{{ URL::asset('js/jquery/validate/jquery.validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery/validate/localization/messages_zh_TW.min.js') }}"></script>
    <!-- Vue js -->
    <script type="text/javascript" src="{{ URL::asset('js/vue/vue.min.js') }}"></script>
    <!-- DataTable Plugin -->
    <link rel="stylesheet" href="{{ URL::asset('css/bs4.datatable/dataTables.bootstrap4.min.css') }}">
    <script type="text/javascript" src="{{ URL::asset('js/bs4.datatable/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/bs4.datatable/dataTables.bootstrap4.min.js') }}"></script>
    <div class="modal" tabindex="-1" role="dialog" id="modalCreate">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">新增報修狀態</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="createForm">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name">故障狀態</label>
                            <input type="text" class="form-control" name="name">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal" tabindex="-1" role="dialog" id="modalUpdate">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">修改報修狀態</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="updateForm">
                    <div class="modal-body">
                        {{ method_field('patch') }}
                        <div class="form-group">
                            <label for="name">故障狀態</label>
                            <input type="text" class="form-control" name="name">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal" tabindex="-1" role="dialog" id="modalDelete">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">刪除報修狀態</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="deleteForm">
                    <div class="modal-body">
                        {{ method_field('delete') }}
                        <table style="width: 100%" id="deleteView">
                            <tr>
                                <td>報修狀態</td>
                                <td>@{{ name }}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" onclick="action_delete()">Save changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div style="padding: 20px;">
        <div id="status-bar">
            <div class="alert alert-success" role="alert" v-show="isSuccess">
                操作成功！
            </div>
            <div class="alert alert-danger" role="alert" v-show="isFailed">
                操作失敗！
            </div>
        </div>
        <button type="button" class="btn btn-default" onclick="modal_create()">新增</button>
        <table id="repairStatusTable" class="table">
            <thead class="thead-dark">
                <th style="text-align: center;font-weight:bold;">狀態</th>
                <th style="text-align: center;font-weight:bold;">操作</th>
            </thead>
            <tbody>
                @foreach($result as $row)
                <tr>
                    <td style="text-align: center;">{{ $row->name }}</td>
                    <td style="text-align: center;">
                        <button type="button" class="btn btn-primary" onclick="modal_update({{ $row->id }})">修改</button>
                        @if($row->id != 1)
                        <button type="button" class="btn btn-danger" onclick="modal_delete({{ $row->id }})">刪除</button>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <script type="text/javascript">
        var data_id = null;
        $('#repairStatusTable').DataTable();
        var status_bar = new Vue({
            el: '#status-bar',
            data:{
                isSuccess: false,
                isFailed: false
            }
        })
        var deleteView = new Vue({
            el: '#deleteView',
            data: {
                name: null
            }
        })
        $('#updateForm').validate({
            submitHandler: function () {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: '{{ url('admin/repair.status') }}/'+data_id,
                    method: 'post',
                    data: $('#updateForm').serialize(),
                    success: function(){
                        Vue.set(status_bar,'isSuccess',true);
                        Vue.set(status_bar,'isFailed',false);
                        setTimeout(function(){
                            location.reload();
                        },5000);
                        $('#modalUpdate').modal('hide');
                    },error: function(){
                        Vue.set(status_bar,'isSuccess',false);
                        Vue.set(status_bar,'isFailed',true);
                        $('#modalUpdate').modal('hide');
                    }
                })
            },
            rules:{
                name: {
                    required: true,
                    minlength: 2,
                    maxlength: 10
                }
            }
        });
        $('#createForm').validate({
            submitHandler: function () {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: '{{ url('admin/repair.status') }}',
                    method: 'post',
                    data: $('#createForm').serialize(),
                    success: function (){
                        Vue.set(status_bar,'isSuccess',true);
                        Vue.set(status_bar,'isFailed',false);
                        setTimeout(function(){
                            location.reload();
                        },5000);
                        $('#modalCreate').modal('hide');
                    },error: function(){
                        Vue.set(status_bar,'isSuccess',false);
                        Vue.set(status_bar,'isFailed',true);
                        $('#modalCreate').modal('hide');
                    }
                })
            },
            rules:{
                name: {
                    required: true,
                    minlength: 2,
                    maxlength: 10
                }
            }
        });
        function action_delete(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '{{ url('admin/repair.status') }}/'+data_id,
                method: 'post',
                data: $('#deleteForm').serialize(),
                success: function(){
                    Vue.set(status_bar,'isSuccess',true);
                    Vue.set(status_bar,'isFailed',false);
                    setTimeout(function(){
                        location.reload();
                    },5000);
                    $('#modalDelete').modal('hide');
                },
                error: function(){
                    Vue.set(status_bar,'isSuccess',false);
                    Vue.set(status_bar,'isFailed',true);
                    $('#modalDelete').modal('hide');
                }
            })
        }
        function modal_create(){
            $('#modalCreate').modal('show');
        }
        function modal_update(id){
            data_id = id;
            $('#modalUpdate').modal('show');
            get_data();
        }
        function modal_delete(id){
            data_id = id;
            $('#modalDelete').modal('show');
            get_data();
        }
        function get_data(){
            $.ajax({
                url: '{{ url('admin/repair.status') }}/'+data_id,
                method: 'get',
                success: function(data){
                    $('input[name="name"]').val(data['name']);
                    Vue.set(deleteView,'name',data['name']);
                },
                error: function(){
                    alert("error");
                }
            })
        }
    </script>
@endsection