@extends('coreui.admintemplate')

@section('title',$webdata->title.'- 設備維修資料管理')

@section('content')
    <!-- Jquery Validate -->
    <script type="text/javascript" src="{{ URL::asset('js/jquery/validate/jquery.validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery/validate/localization/messages_zh_TW.min.js') }}"></script>
    <!-- Vue js -->
    <script type="text/javascript" src="{{ URL::asset('js/vue/vue.min.js') }}"></script>
    <!-- DataTable Plugin -->
    <link rel="stylesheet" href="{{ URL::asset('css/bs4.datatable/dataTables.bootstrap4.min.css') }}">
    <script type="text/javascript" src="{{ URL::asset('js/bs4.datatable/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/bs4.datatable/dataTables.bootstrap4.min.js') }}"></script>
    <div class="modal" tabindex="-1" role="dialog" id="updateModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">更新報修資料</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="updateForm">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="year_id">學年度資料集</label>
                            <select class="form-control" name="year_id">
                                @foreach($school_year as $row)
                                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="category">設備分類</label>
                            <select class="form-control" name="category" onchange="loading_name(this,1)">
                                @foreach($category as $row)
                                <option value="{{ $row->id }}">{{ $row->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="device_name">設備名稱</label>
                            <select class="form-control" name="device_name">
                                @foreach($device_name as $row)
                                <option value="{[ $row->id }}">{{ $row->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="location">地點</label>
                            <input type="text" name="location" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="content">故障內容</label>
                            <textarea class="form-control" name="content">
                            </textarea>
                        </div>
                        <div class="form-group">
                            <label for="fault">故障分類</label>
                            <select name="fault" class="form-control">
                                @foreach($fault as $row)
                                <option value="{{ $row->id }}">{{ $row->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <h3 style="text-align: center;">檢修狀態</h3><hr>
                        <div class="form-group">
                            <label for="status">維修狀態</label>
                            <select name="status" class="form-control">
                                @foreach($repair_status as $row)
                                <option value="{{ $row->id }}">{{ $row->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="description">檢修狀態回報</label>
                            <textarea name="description" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">更新資料</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal" tabindex="-1" role="dialog" id="deleteModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">刪除報修資料</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="deleteForm">
                    <div class="modal-body">
                        {{ method_field('delete') }}
                        <table style="text-align: center;width: 100%;" id="deleteView">
                            <tr>
                                <td>資料集</td>
                                <td>@{{ dataset }}</td>
                            </tr>
                            <tr>
                                <td>設備分類</td>
                                <td>@{{ category }}</td>
                            </tr>
                            <tr>
                                <td>設備名稱</td>
                                <td>@{{ device_name }}</td>
                            </tr>
                            <tr>
                                <td>地點</td>
                                <td>@{{ location }}</td>
                            </tr>
                            <tr>
                                <td>故障分類</td>
                                <td>@{{ fault }}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" onclick="action_delete()">刪除</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div style="padding: 20px;">
        <div id="status-bar">
            <div class="alert alert-success" role="alert" v-show="isSuccess">
                操作成功！
            </div>
            <div class="alert alert-danger" role="alert" v-show="isFailed">
                操作失敗！
            </div>
        </div>
        <!-- 即時搜尋 -->
        <table width="60%">
            <tr>
                <td width="20%">
                    <div class="form-group">
                        <label for="yearSearch">學年度</label>
                        <select id="yearSearch" class="form-control" onchange="loading_name(this)">
                            <option selected>全資料</option>
                            @foreach($school_year as $row)
                                <option value="{{ $row->id }}" >{{ $row->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </td>
                <td width="20%">
                    <div class="form-group">
                        <label for="category">設備分類</label>
                        <select id="categorySearch" class="form-control" onchange="loading_name(this)">
                            <option selected></option>
                            @foreach($category as $row)
                                <option value="{{ $row->id }}" >{{ $row->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </td>
                <td width="20%">
                    <div class="form-group">
                        <label for="name">設備名稱</label>
                        <select id="nameSearch" class="form-control">
                            <option selected></option>
                        </select>
                    </div>
                </td>
                <td width="20%">
                    <div class="form-group">
                        <label for="location">地點</label>
                        <input type="text" id="locationSearch" class="form-control">
                    </div>
                </td>
                <td width="20%">
                    <div class="form-group">
                        <label for="status">狀態</label>
                        <select id="statusSearch" class="form-control">
                            <option selected></option>
                            @foreach($repair_status as $row)
                                <option value="{{ $row->name }}" >{{ $row->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </td>
            </tr>
        </table>
        <table class="table" id="repairTable">
            <thead class="thead-dark">
                <th style="text-align: center;font-weight:bold; display: none;">年度資料編號</th>
                <th style="text-align: center;font-weight:bold; display: none;">設備分類編號</th>
                <th style="text-align: center;font-weight:bold; display: none;">設備名稱編號</th>
                <th style="text-align: center;font-weight:bold;">年度資料</th>
                <th style="text-align: center;font-weight:bold;">設備分類</th>
                <th style="text-align: center;font-weight:bold;">設備名稱</th>
                <th style="text-align: center;font-weight:bold;">地點</th>
                <th style="text-align: center;font-weight:bold;">故障敘述</th>
                <th style="text-align: center;font-weight:bold;">故障分類</th>
                <th style="text-align: center;font-weight:bold;">狀態</th>
                <th style="text-align: center;font-weight:bold;">處理情形</th>
                <th style="text-align: center;font-weight:bold;">建立時間</th>
                <th style="text-align: center;font-weight:bold;">修改時間</th>
                <th style="text-align: center;font-weight:bold;">操作</th>
            </thead>
            <tbody>
            @foreach($result as $row)
                <tr>
                    <td style="text-align: center; display: none;">{{ $row->school_year->id }}</td>
                    <td style="text-align: center; display: none;">{{ $row->device_category->id }}</td>
                    <td style="text-align: center; display: none;">{{ $row->device_name->id }}</td>
                    <td style="text-align: center;">{{ $row->school_year->name }}</td>
                    <td style="text-align: center;">{{ $row->device_category->name }}</td>
                    <td style="text-align: center;">{{ $row->device_name->name }}</td>
                    <td style="text-align: center;">{{ $row->location }}</td>
                    <td style="text-align: center;">{{ $row->content }}</td>
                    <td style="text-align: center;">{{ $row->fault_category->name }}</td>
                    <td style="text-align: center;">{{ $row->repair_status->name }}</td>
                    <td style="text-align: center;">{{ $row->description }}</td>
                    <td style="text-align: center;">{{ $row->created_at->format('Y-m-d H;i:s') }}</td>
                    <td style="text-align: center;">{{ $row->updated_at->format('Y-m-d H;i:s') }}</td>
                    <td style="text-align: center;">
                        <button type="button" class="btn btn-primary" onclick="modal_update({{ $row->id }})">修改</button>
                        <button type="button" class="btn btn-danger" onclick="modal_delete({{ $row->id }})">刪除</button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <script type="text/javascript">
        var data_id;
        $(document).ready(function(){
            $('#repairTable_filter').hide();
        })
        var status_bar = new Vue({
            el: '#status-bar',
            data:{
                isSuccess: false,
                isFailed: false
            }
        })
        var deleteView = new Vue({
            el: '#deleteView',
            data: {
                fault: null,
                category: null,
                device_name: null,
                location: null,
                dataset: null
            }
        })
        var table = $('#repairTable').DataTable();
        $('#yearSearch').change(function(){
            table.columns(0)
                .search($('#yearSearch').val())
                .draw();
        })
        $('#statusSearch').change(function(){
            table.columns(9)
                .search($('#statusSearch').val())
                .draw();
        })
        $('#nameSearch').change(function(){
            table.columns(2)
                .search($('#nameSearch').val())
                .draw();
        })
        $('#locationSearch').keyup(function(){
            table.columns(6)
                .search($('#locationSearch').val())
                .draw();
        })
        $('#updateForm').validate({
            submitHandler: function(){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: '',
                    data: $('#updateForm').serialize(),
                    method: 'post',
                    success: function(data){
                        Vue.set(status_bar,'isSuccess',true);
                        Vue.set(status_bar,'isFailed',false);
                        setTimeout(function(){
                            location.reload()
                        },5000);
                        $('#updateModal').modal('hide');
                    },error: function(){
                        Vue.set(status_bar,'isSuccess',false);
                        Vue.set(status_bar,'isFailed',true);
                        $('#updateModal').modal('hide');
                    }
                })
            },roles: {
                year_id: 'required',
                category: 'required',
                device_name: 'required',
                location: {
                    required: true,
                    minlength: 2,
                    maxlength: 10
                },
                fault: 'required',
                status: 'required'
            }
        });
        function modal_update(id){
            data_id = id;
            get_data();
            $('#updateModal').modal('show');
        }
        function modal_delete(id){
            data_id = id;
            get_data();
            $('#deleteModal').modal('show');
        }
        function action_delete(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '{{ url('admin/device.repair') }}/'+data_id,
                data: $('#deleteForm').serialize(),
                method: 'post',
                success: function(){
                    Vue.set(status_bar,'isSuccess',true);
                    Vue.set(status_bar,'isFailed',false);
                    setTimeout(function(){
                        location.reload()
                    },5000);
                    $('#deleteModal').modal('hide');
                },error: function(){
                    Vue.set(status_bar,'isSuccess',false);
                    Vue.set(status_bar,'isFailed',true);
                    $('#deleteModal').modal('hide');
                }
            })
        }
        function get_data(){
            $.ajax({
                url: '{{ url('admin/device.repair')  }}/'+data_id,
                method: 'get',
                success: function(data){
                    update_nameSelect(data['device_category_id'],data['device_name_id']);
                    update_faultSetect(data['device_name_id'],data['fault_category_id']);
                    $('select[name="category"]').val(data['device_category_id']);
                    $('select[name="year_id"]').val(data['school_year_id']);
                    $('select[name="status"]').val(data['repair_status_id']);
                    $('input[name="location"]').val(data['location']);
                    $('textarea[name="content"]').val(data['content']);
                    $('textarea[name="description"]').val(data['description']);
                    Vue.set(deleteView,'category',data['joinData']['category']);
                    Vue.set(deleteView,'device_name',data['joinData']['device_name']);
                    Vue.set(deleteView,'fault',data['joinData']['fault']);
                    Vue.set(deleteView,'location',data['location']);
                    Vue.set(deleteView,'dataset',data['joinData']['school_year']);
                },
                error: function(){
                    alert("擷取該筆資料發生錯誤!");
                }
            })
        }
        function loading_name(event,search = 0){
            $.ajax({
                url: '{{ url('ajax/device.name') }}/'+event.value,
                success: function(data){
                    $('select[name="device_name"]').empty();
                    $('#nameSearch').empty();
                    $('#nameSearch').append(new Option('','',true));
                    $.each(data,function (index,value) {
                        $('select[name="device_name"]').append(new Option(value['name'], value['id']));
                        $('#nameSearch').append(new Option(value['name'], value['id']))
                    })
                }
            })
            if(search != 0){
                table.columns(1).search( event.value ).draw();
            }
        }
        function update_nameSelect(id,defaultTarget){
            $.ajax({
                url: '{{ url('ajax/device.name') }}/'+id,
                method: 'get',
                success: function(data){
                    $('select[name="device_name"]').empty();
                    $.each(data,function (index,value) {
                        $('select[name="device_name"]').append(new Option(value['name'],value['id']));
                    })
                    $('select[name="device_name"]').val(defaultTarget);
                }
            })
        }
        function update_faultSetect(id,defaultTarget){
            $.ajax({
                url: '{{ url('ajax/fault.category') }}/'+id,
                method: 'get',
                success: function(data){
                    $('select[name="fault"]').empty();
                    $.each(data,function (index,value) {
                        $('select[name="fault"]').append(new Option(value['name'],value['id']));
                    })
                    $('select[name="fault"]').val(defaultTarget);
                }
            })
        }

    </script>
@endsection