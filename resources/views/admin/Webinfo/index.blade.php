@extends('coreui.admintemplate')

@section('title',$webdata->title." - 網站基本資訊管理")

@section('content')
    <div style="padding: 5%;">
        <form action="{{ url('admin/webinfo') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="title">網站標題</label>
                <input type="text" name="title" class="form-control" value="{{ $webdata->title }}" required>
            </div>
            <div class="form-group">
                <label for="keyword">關鍵字</label>
                <input type="text" name="keyword" class="form-control" value="{{ $webdata->keyword }}" >
            </div>
            <div class="form-group">
                <label for="description">敘述</label>
                <textarea name="description" class="form-control">
                {{ $webdata->description }}
                </textarea>
            </div>
            <button type="submit" name="submit" class="btn btn-default">送出</button>
        </form>
    </div>
@endsection