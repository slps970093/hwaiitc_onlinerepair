@extends('coreui.admintemplate')

@section('title',$webdata->title." - 學年度資料集管理")

@section('content')
    <!-- Jquery Validate -->
    <script type="text/javascript" src="{{ URL::asset('js/jquery/validate/jquery.validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery/validate/localization/messages_zh_TW.min.js') }}"></script>
    <!-- Vue js -->
    <script type="text/javascript" src="{{ URL::asset('js/vue/vue.min.js') }}"></script>
    <!-- DataTable Plugin -->
    <link rel="stylesheet" href="{{ URL::asset('css/bs4.datatable/dataTables.bootstrap4.min.css') }}">
    <script type="text/javascript" src="{{ URL::asset('js/bs4.datatable/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/bs4.datatable/dataTables.bootstrap4.min.js') }}"></script>
    <div class="modal" tabindex="-1" role="dialog" id="createModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">新增學年度資料集</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="createFrom">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name">資料集名稱</label>
                            <input type="text" name="name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="isEnable">是否設定為預設資料集</label>
                            <select class="form-control" name="isEnable">
                                <option value="1">預設資料</option>
                                <option value="0" selected>非預設資料</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="start_date">開始日期</label>
                            <input type="date" name="start_date" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="end_date">結束日期</label>
                            <input type="date" name="end_date" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">送出</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal" tabindex="-1" role="dialog" id="updateModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">修改學年度資料集</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="updateForm">
                    <div class="modal-body">
                        {{ method_field("patch") }}
                        <div class="form-group">
                            <label for="name">資料集名稱</label>
                            <input type="text" name="name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="isEnable">是否設定為預設資料集</label>
                            <select class="form-control" name="isEnable">
                                <option value="1">預設資料</option>
                                <option value="0" selected>非預設資料</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="start_date">開始日期</label>
                            <input type="date" name="start_date" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="end_date">結束日期</label>
                            <input type="date" name="end_date" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">送出</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal" tabindex="-1" role="dialog" id="deleteModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">刪除學年度資料集</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="deleteFrom">
                    <div class="modal-body">
                        <div id="deleteView">
                            {{ method_field('delete') }}
                            <h4>注意：刪除後資料將不可逆，系統會刪除所有相關連的資料</h4>
                            <table style="width: 100%;">
                                <tr>
                                    <td>資料集名稱</td>
                                    <td>@{{ name }}</td>
                                </tr>
                                <tr>
                                    <td>報修開始日期</td>
                                    <td>@{{ start_date }}</td>
                                </tr>
                                <tr>
                                    <td>報修截止日期</td>
                                    <td>@{{ end_date }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" onclick="action_delete()" class="btn btn-danger">刪除資料</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div style="padding: 20px;">
        <div id="status-bar">
            <div class="alert alert-success" role="alert" v-show="isSuccess">
                操作成功！
            </div>
            <div class="alert alert-danger" role="alert" v-show="isFailed">
                操作失敗！
            </div>
        </div>
        <button type="button" onclick="modal_create()" class="btn btn-default">新增</button>
        <table class="table" id="yearTable">
            <thead class="thead-dark">
                <th style="text-align: center;font-weight:bold;">資料集</th>
                <th style="text-align: center;font-weight:bold;">是否預設資料</th>
                <th style="text-align: center;font-weight:bold;">開始日期</th>
                <th style="text-align: center;font-weight:bold;">結束日期</th>
                <th style="text-align: center;font-weight:bold;">操作</th>
            </thead>
            <tbody>
                @foreach($result as $row)
                <tr>
                    <td style="text-align: center;font-weight:bold;">{{ $row->name }}</td>
                    <td style="text-align: center;font-weight:bold;">
                        @if((boolean) $row->isEnable)
                        是
                        @else
                        否
                        @endif
                    </td>
                    <td style="text-align: center;font-weight:bold;">{{ $row->start_date }}</td>
                    <td style="text-align: center;font-weight:bold;">{{ $row->end_date }}</td>
                    <td style="text-align: center;font-weight:bold;">
                        <button type="button" onclick="modal_update({{$row->id}})" class="btn btn-primary">修改</button>
                        <button type="button" onclick="modal_delete({{ $row->id }})" class="btn btn-danger">刪除</button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <script type="text/javascript">
        var data_id = null;
        $('#yearTable').DataTable();
        var data_id = null;
        var status_bar = new Vue({
            el: '#status-bar',
            data:{
                isSuccess: false,
                isFailed: false
            }
        })
        var deleteView = new Vue({
            el: '#deleteView',
            data: {
                start_date: null,
                end_date: null,
                name: null
            }
        });
        function modal_create(){
            $('#createModal').modal('show');
        }
        function modal_update(id){
            $('#updateModal').modal('show');
            data_id = id;
            get_data();
        }
        function modal_delete(id){
            $('#deleteModal').modal('show');
            data_id = id;
            get_data();
        }
        function action_delete(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '{{ url('admin/school.year') }}/'+data_id,
                method: 'post',
                success: function(){
                    Vue.set(status_bar,'isSuccess',true);
                    Vue.set(status_bar,'isFailed',false);
                    setTimeout(function(){
                        location.reload();
                    },5000);
                },error: function(){
                    Vue.set(status_bar,'isSuccess',false);
                    Vue.set(status_bar,'isFailed',true);
                }
            })
        }
        function get_data(){
            $.ajax({
                url: '{{ url('admin/school.year') }}/'+data_id,
                method: 'get',
                success: function(data){
                    $('input[name="name"]').val(data['name']);
                    $('input[name="start_date"]').val(data['start_date']);
                    $('input[name="end_date"]').val(data['end_date']);
                    $('select[name="isEnable"]').val(data['isEnable']);
                    Vue.set(deleteView,'start_date',data['start_date']);
                    Vue.set(deleteView,'end_date',data['end_date']);
                    Vue.set(deleteView,'name',data['name']);
                }
            })
        }
        $('#createFrom').validate({
            submitHandler: function(){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: '{{ url('admin/school.year') }}',
                    method: 'post',
                    data: $('#createFrom').serialize(),
                    success: function(){
                        Vue.set(status_bar,'isSuccess',true);
                        Vue.set(status_bar,'isFailed',false);
                        setTimeout(function(){
                            location.reload();
                        },5000);
                        $('#createModal').modal('hide');
                    },
                    error: function(){
                        Vue.set(status_bar,'isSuccess',false);
                        Vue.set(status_bar,'isFailed',true);
                        $('#createModal').modal('hide');
                    }
                })
            },rules: {
                name: {
                    required: true,
                    minlength: 2,
                    maxlength: 10
                },
                isEnable: 'required',
                start_date: {
                    required: true,
                    date: true
                },
                end_date: {
                    required: true,
                    date: true
                }
            }
        })
        $('#updateForm').validate({
            submitHandler: function(){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: '{{ url('admin/school.year') }}/'+data_id,
                    method: 'post',
                    data: $('#updateForm').serialize(),
                    success: function(){
                        Vue.set(status_bar,'isSuccess',true);
                        Vue.set(status_bar,'isFailed',false);
                        setTimeout(function(){
                            location.reload();
                        },5000);
                        $('#updateModal').modal('hide');
                    },error: function(){
                        Vue.set(status_bar,'isSuccess',false);
                        Vue.set(status_bar,'isFailed',true);
                        $('#updateModal').modal('hide');
                    }
                })
            },rules: {
                name: {
                    required: true,
                    minlength: 2,
                    maxlength: 10
                },
                isEnable: 'required',
                start_date: {
                    required: true,
                    date: true
                },
                end_date: {
                    required: true,
                    date: true
                }
            }
        })
    </script>
@endsection