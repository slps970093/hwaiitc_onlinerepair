@extends('coreui.admintemplate')

@section('title',$webdata->title.'- 故障分類管理')

@section('content')
    <!-- Jquery Validate -->
    <script type="text/javascript" src="{{ URL::asset('js/jquery/validate/jquery.validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery/validate/localization/messages_zh_TW.min.js') }}"></script>
    <!-- Vue js -->
    <script type="text/javascript" src="{{ URL::asset('js/vue/vue.min.js') }}"></script>
    <!-- DataTable Plugin -->
    <link rel="stylesheet" href="{{ URL::asset('css/bs4.datatable/dataTables.bootstrap4.min.css') }}">
    <script type="text/javascript" src="{{ URL::asset('js/bs4.datatable/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/bs4.datatable/dataTables.bootstrap4.min.js') }}"></script>
    <!-- Modal Create -->
    <div class="modal" tabindex="-1" role="dialog" id="modalCreate">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">新增故障分類</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="createForm">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="category">設備分類</label>
                            <select name="category" class="form-control" onchange="loading_device_name(this)">
                                <option selected></option>
                                @foreach($category as $row)
                                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="extends">設備名稱</label>
                            <select name="extends" class="form-control">
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="name">故障分類</label>
                            <input type="text" name="name" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal" tabindex="-1" role="dialog" id="modalUpdate">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">修改故障分類</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="updateForm">
                    <div class="modal-body">
                        {{ method_field('patch') }}
                        <div class="form-group">
                            <label for="category">設備分類</label>
                            <select name="category" class="form-control" onchange="loading_device_name(this)">
                                <option selected></option>
                                @foreach($category as $row)
                                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="extends">設備名稱</label>
                            <select name="extends" class="form-control">
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="name">故障分類</label>
                            <input type="text" name="name" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">送出</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal" tabindex="-1" role="dialog" id="modalDelete">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">刪除故障分類</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="deleteView">
                    <div class="modal-body">
                        {{ method_field('delete') }}
                        <h4>注意：刪除後資料將不可逆，系統會刪除所有相關連的資料</h4>
                        <table style="width: 100%;" id="deleteView">
                            <tr>
                                <td>設備名稱</td>
                                <td>@{{ extends_name }}</td>
                            </tr>
                            <tr>
                                <td>故障分類</td>
                                <td>@{{ name }}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger">刪除</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div style="padding: 20px;">
        <div id="status-bar">
            <div class="alert alert-success" role="alert" v-show="isSuccess">
                操作成功！
            </div>
            <div class="alert alert-danger" role="alert" v-show="isFailed">
                操作失敗！
            </div>
        </div>
        <button type="button" class="btn btn-default" onclick="modal_create()">新增</button><p>
        <table class="table" id="faultTable">
            <thead class="thead-dark">
                <th style="text-align: center;font-weight:bold;">設備名稱</th>
                <th style="text-align: center;font-weight:bold;">故障分類</th>
                <th style="text-align: center;font-weight:bold;">操作</th>
            </thead>
            <tbody>
                @foreach($result as $row)
                <tr>
                    <td style="text-align: center;">{{ $row->device_name->name }}</td>
                    <td style="text-align: center;">{{ $row->name }}</td>
                    <td style="text-align: center;">
                        <button type="button" class="btn btn-primary" onclick="modal_update({{ $row->id }})">修改</button>
                        <button type="button" class="btn btn-danger" onclick="modal_delete({{ $row->id }})">刪除</button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <script type="text/javascript">
            var data_id;
            var deleteView = new Vue({
                el: '#deleteView',
                data: {
                    name: null,
                    extends_name: null
                }
            })
            var status_bar = new Vue({
                el: '#status-bar',
                data:{
                    isSuccess: false,
                    isFailed: false
                }
            })
            $('#faultTable').DataTable();
            function  modal_create() {
                $('#modalCreate').modal('show');
            }
            function modal_update(id){
                data_id = id;
                $('#modalUpdate').modal('show');
                get_data()
            }
            function modal_delete(id){
                data_id = id;
                $('#modalDelete').modal('show');
                get_data();
            }
            function get_data(){
                $.ajax({
                    url: '{{ url('admin/fault.category') }}/'+data_id,
                    method: 'get',
                    success: function(data){
                        $('input[name="name"]').val(data['name']);
                        $('select[name="category"]').val(data['category']);
                        Vue.set(deleteView,'name',data['name']);
                        Vue.set(deleteView,'extends_name',data['device_name']);
                        set_device_name(data['extends_name'])
                    },
                    error: function(){
                        alert('ERROR');
                    }
                })
            }
            function set_device_name(defaultValue){
                $.ajax({
                    url: '{{ url('ajax/device.name') }}/'+$('select[name="category"]').val(),
                    success: function(data){
                        $('select[name="extends"]').empty();
                        $('select[name="extends"]').empty();
                        $('select[name="extends"]').append(new Option('','',true));
                        $.each(data,function (index,value) {
                            $('select[name="extends"]').append(new Option(value['name'], value['id']));
                        })
                        $('select[name="extends"]').val(defaultValue);
                    }
                })
            }
            $('#createForm').validate({
                submitHandler: function () {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: '{{ url('admin/fault.category') }}',
                        method: 'post',
                        data: $('#createForm').serialize(),
                        success: function(){
                            setTimeout(function(){
                                location.reload();
                            },5000);
                            Vue.set(status_bar,'isSuccess',true);
                            Vue.set(status_bar,'isFailed',false);
                            $('#modalCreate').modal('hide');
                        },error: function(){
                            Vue.set(status_bar,'isSuccess',false);
                            Vue.set(status_bar,'isFailed',true);
                            $('#modalCreate').modal('hide');
                        }
                    })
                },roles:{
                    extends: 'required',
                    name: {
                        required : true,
                        maxlength: 15,
                        minlength: 2
                    }
                }
            })
            function loading_device_name(event){
                $.ajax({
                    url: '{{ url('ajax/device.name') }}/'+event.value,
                    success: function(data){
                        $('select[name="extends"]').empty();
                        $('select[name="extends"]').empty();
                        $('select[name="extends"]').append(new Option('','',true));
                        $.each(data,function (index,value) {
                            $('select[name="extends"]').append(new Option(value['name'], value['id']));
                        })
                    }
                })
            }
            $('#updateForm').validate({
                submitHandler: function(){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: '{{ url('admin/fault.category') }}',
                        method: 'post',
                        data: $('#updateForm').serialize(),
                        success: function(){
                            setTimeout(function(){
                                location.reload();
                            },5000);
                            Vue.set(status_bar,'isSuccess',true);
                            Vue.set(status_bar,'isFailed',false);
                            $('#modalUpdate').modal('hide');
                        },error: function(){
                            Vue.set(status_bar,'isSuccess',false);
                            Vue.set(status_bar,'isFailed',true);
                            $('#modalUpdate').modal('hide');
                        }
                    })
                },roles:{
                    extends: 'required',
                    name: {
                        required : true,
                        maxlength: 15,
                        minlength: 2
                    }
                }
            })
            function action_delete(){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: '{{ url('admin/fault.category') }}/'+data_id,
                    method: 'post',
                    data: $('#deleteModal').serialize(),
                    success: function(){
                        Vue.set(status_bar,'isSuccess',true);
                        Vue.set(status_bar,'isFailed',false);
                        setTimeout(function(){
                            location.reload();
                        },5000);
                        $('#modalDelete').modal('hide');
                    },error: function () {
                        Vue.set(status_bar,'isSuccess',false);
                        Vue.set(status_bar,'isFailed',true);
                        $('#modalDelete').modal('hide');
                    }
                })
            }
        </script>
    </div>
@endsection