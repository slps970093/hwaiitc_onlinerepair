@extends('coreui.admintemplate')

@section('title',$webdata->title.'- 華醫科大校園財產登錄模組')

@section('content')
    <!-- Jquery Validate -->
    <script type="text/javascript" src="{{ URL::asset('js/jquery/validate/jquery.validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery/validate/localization/messages_zh_TW.min.js') }}"></script>
    <!-- Vue js -->
    <script type="text/javascript" src="{{ URL::asset('js/vue/vue.min.js') }}"></script>
    <!-- DataTable Plugin -->
    <link rel="stylesheet" href="{{ URL::asset('css/bs4.datatable/dataTables.bootstrap4.min.css') }}">
    <script type="text/javascript" src="{{ URL::asset('js/bs4.datatable/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/bs4.datatable/dataTables.bootstrap4.min.js') }}"></script>
    <!-- file upload -->
    <script type="text/javascript" src="{{ URL::asset('js/jquery/fileupload/jquery.iframe-transport.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery/fileupload/vendor/jquery.ui.widget.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery/fileupload/jquery.fileupload.js') }}"></script>
    <div class="modal" tabindex="-1" role="dialog" id="deleteModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">確認是否刪除資料</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="deleteForm">
                        {{ method_field('delete') }}
                    </form>
                    刪除後資料將不可回復，如需刪除請按下刪除</br>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" onclick="action_delete()">刪除</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- 檔案上傳 -->
    <div class="modal" tabindex="-1" role="dialog" id="uploadModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">設備資料匯入作業</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="fileuploadForm" enctype="multipart/form-data" action="{{ url('admin/module/hwai/drivelist/import') }}">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="file">匯入檔案(限制 *.XLSX)</label>
                            <input type="file" name="file" class="form-control" accept=".xlsx">
                        </div>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div style="padding: 5px;">
        <div class="btn-group" role="group" aria-label="Basic example">
            <button type="button" class="btn btn-secondary" onclick="location.href='{{ url('admin/module/hwai/drivelist/create') }}'">新增</button>
            <button type="button" class="btn btn-secondary" onclick="$('#uploadModal').modal('show');">匯入</button>
            <button type="button" class="btn btn-secondary" onclick="semple_file()">產生空白範本匯入檔</button>
            <button type="button" class="btn btn-secondary" onclick="location.href='{{ url('admin/module/hwai/drivelist/category') }}'">設備分類代碼表</button>
            <button type="button" class="btn btn-secondary" onclick="location.href='{{ url('admin/module/hwai/drivelist/export') }}'">匯出</button>
        </div>
        <table class="table" id="myTable">
            <thead class="thead-dark">
            <th style="text-align: center;font-weight:bold;">財產序號</th>
            <th style="text-align: center;font-weight:bold;">起</th>
            <th style="text-align: center;font-weight:bold;">迄</th>
            <th style="text-align: center;font-weight:bold;">舊財編</th>
            <th style="text-align: center;font-weight:bold;">設備分類</th>
            <th style="text-align: center;font-weight:bold;">設備名稱</th>
            <th style="text-align: center;font-weight:bold;">財產名稱</th>
            <th style="text-align: center;font-weight:bold;">廠牌</th>
            <th style="text-align: center;font-weight:bold;">型號</th>
            <th style="text-align: center;font-weight:bold;">規格</th>
            <th style="text-align: center;font-weight:bold;">數量</th>
            <th style="text-align: center;font-weight:bold;">單位</th>
            <th style="text-align: center;font-weight:bold;">原始地點</th>
            <th style="text-align: center;font-weight:bold;">目前地點</th>
            <th style="text-align: center;font-weight:bold;">使用年限</th>
            <th style="text-align: center;font-weight:bold;">保管人</th>
            <th style="text-align: center;font-weight:bold;">操作</th>
            </thead>
            <tbody>
            @foreach($result as $row)
                <tr>
                    <td style="text-align: center;">{{ $row->serial }}</td>
                    <td style="text-align: center;">{{ $row->start }}</td>
                    <td style="text-align: center;">{{ $row->until }}</td>
                    <td style="text-align: center;">{{ $row->old_serial }}</td>
                    <td style="text-align: center;">{{ $row->device_category->name }}</td>
                    <td style="text-align: center;">{{ $row->device_name->name }}</td>
                    <td style="text-align: center;">{{ $row->name }}</td>
                    <td style="text-align: center;">{{ $row->brand }}</td>
                    <td style="text-align: center;">{{ $row->model }}</td>
                    <td style="text-align: center;">{{ $row->specification }}</td>
                    <td style="text-align: center;">{{ $row->qty }}</td>
                    <td style="text-align: center;">{{ $row->unit }}</td>
                    <td style="text-align: center;">{{ $row->original_location }}</td>
                    <td style="text-align: center;">{{ $row->current_location }}</td>
                    <td style="text-align: center;">{{ $row->year }}</td>
                    <td style="text-align: center;">{{ $row->custodian }}</td>
                    <td style="text-align: center;">
                        <button type="button" class="btn btn-primary" onclick="location.href='{{ url('admin/module/hwai/drivelist/'.$row->id.'/update') }}'">修改</button>
                        <button type="button" class="btn btn-danger" onclick="modal_delete({{ $row->id }})">刪除</button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <script type="text/javascript">
        $('#myTable').DataTable();
        var data_id = null;
        function modal_delete(id){
            data_id = id;
            $('#deleteModal').modal('show');
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#fileuploadForm').fileupload({
            method: 'post',
            dataType: 'json',
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('.progress-bar').css(
                    'width',
                    progress + '%'
                );
                $('.progress-bar').attr('aria-valuenow',progress);
                $('.progress-bar').text(progress + '%');
            },
            done: function(){
                alert("匯入作業完成，將自動重新整理頁面!");
                location.reload();
            },
            error: function(){
                alert("error");
            }
        });
        function action_delete(){
            $.ajax({
                url: '{{ url('ajax/device.name') }}/'+id,
                method: 'get',
                success: function(data){
                    $('select[name="device_name"]').empty();
                    $.each(data,function (index,value) {
                        $('select[name="device_name"]').append(new Option(value['name'],value['id']));
                    })
                    $('select[name="device_name"]').val(defaultTarget);
                }
            })
        }
        function semple_file(){
            if(confirm("請注意！要進行匯出作業時，請務必將 設備分類(請填入代碼) 欄位 填入系統對應之代碼 (可到 設備分類代碼表 查詢，一個財產只能對應單一代碼)，否則匯入會失敗")){
                window.open('{{ url('admin/module/hwai/drivelist/sample') }}')
            }
        }
        function action_delete(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '{{ url('admin/module/hwai/drivelist') }}/'+data_id,
                data: $('#deleteForm').serialize(),
                method: 'post',
                success: function(){
                    $('#deleteModal').modal('hide');
                    setTimeout(function(){
                        location.reload();
                    },2000);
                    alert("資料成功刪除");
                },
                error: function(){
                    $('#deleteModal').modal('hide');
                    alert("資料刪除失敗");
                }
            })
        }
    </script>
@endsection