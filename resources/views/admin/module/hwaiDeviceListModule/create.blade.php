@extends('coreui.admintemplate')

@section('title',$webdata->title.'- 華醫科大校園財產登錄模組 新增資料')

@section('content')
    <!-- Jquery Validate -->
    <script type="text/javascript" src="{{ URL::asset('js/jquery/validate/jquery.validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery/validate/localization/messages_zh_TW.min.js') }}"></script>
    <!-- Vue js -->
    <script type="text/javascript" src="{{ URL::asset('js/vue/vue.min.js') }}"></script>
    <!-- DataTable Plugin -->
    <link rel="stylesheet" href="{{ URL::asset('css/bs4.datatable/dataTables.bootstrap4.min.css') }}">
    <script type="text/javascript" src="{{ URL::asset('js/bs4.datatable/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/bs4.datatable/dataTables.bootstrap4.min.js') }}"></script>
    <div style="padding: 20px;">
        <h2 style="text-align: center;">新增財產資料</h2>
        <hr>
        <form action="{{ url('admin/module/hwai/drivelist') }}" method="post">
            {{ csrf_field() }}
            <table style="width: 100%">
                <tr>
                    <td>
                        <div class="form-group">
                            <label for="serial">財產序號</label>
                            <input type="text" name="serial" class="form-control" required>
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <label for="start">起</label>
                            <input type="number" name="start" class="form-control" required>
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <label for="start">迄</label>
                            <input type="number" name="until" class="form-control" required>
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <label for="old_serial">舊財編</label>
                            <input type="text" name="old_serial" class="form-control" >
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <label for="device_category">設備分類</label>
                        <select name="device_category" class="form-control" onchange="loading_device_name()" required>
                            <option selected="selected">please select</option>
                            @foreach($category as $row)
                                <option value="{{ $row->id }}">{{ $row->name }}</option>
                            @endforeach
                        </select>
                    </td>
                    <td colspan="2">
                        <label for="device_name">設備名稱</label>
                        <select name="device_name" class="form-control" required>
                        </select></td>
                </tr>
            </table>
            <div class="form-group">
                <label for="name">財產名稱</label>
                <input type="text" name="name" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="brand">廠牌</label>
                <input type="text" name="brand" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="model">型號</label>
                <input type="text" name="model" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="specification">規格</label>
                <textarea name="specification" class="form-control"></textarea>
            </div>
            <div class="form-group">
                <label for="qty">數量</label>
                <input type="number" name="qty" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="unit">單位</label>
                <input type="text" name="unit" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="original_location">原始地點</label>
                <input type="text" name="original_location" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="current_location">目前地點</label>
                <input type="text" name="current_location" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="year">年限</label>
                <input type="number" name="year" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="custodian">保管人</label>
                <input type="text" name="custodian" class="form-control" required>
            </div>
            <button type="submit" class="btn btn-default">送出</button>
        </form>
        <script type="text/javascript">
            function loading_device_name(){
                $.ajax({
                    url: '{{ url('ajax/device.name') }}/'+$('select[name="device_category"]').val(),
                    method: 'get',
                    success: function(data){
                        $('select[name="device_name"]').empty();
                        $.each(data,function (index,value) {
                            $('select[name="device_name"]').append(new Option(value['name'],value['id']));
                        })
                    },error:function(){
                        alert("載入資料發生錯誤！");
                    }
                })
            }
        </script>
    </div>

@endsection