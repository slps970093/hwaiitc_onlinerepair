@extends('coreui.admintemplate')

@section('title',$webdata->title." - 設備分類管理")

@section('content')
    <!-- Jquery Validate -->
    <script type="text/javascript" src="{{ URL::asset('js/jquery/validate/jquery.validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery/validate/localization/messages_zh_TW.min.js') }}"></script>
    <!-- Vue js -->
    <script type="text/javascript" src="{{ URL::asset('js/vue/vue.min.js') }}"></script>
    <!-- DataTable Plugin -->
    <link rel="stylesheet" href="{{ URL::asset('css/bs4.datatable/dataTables.bootstrap4.min.css') }}">
    <script type="text/javascript" src="{{ URL::asset('js/bs4.datatable/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/bs4.datatable/dataTables.bootstrap4.min.js') }}"></script>
    <!-- Modal Create -->
    <div class="modal" tabindex="-1" role="dialog" id="createModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">新增設備分類</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="createForm">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name">設備分類名稱</label>
                            <input type="text" name="name" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">送出</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Modal update -->
    <div class="modal" tabindex="-1" role="dialog" id="updateModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">修改設備分類</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="updateForm">
                    <div class="modal-body">
                        {{ method_field('patch') }}
                        <div class="form-group">
                            <label for="name">設備分類名稱</label>
                            <input type="text" name="name" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">送出</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Modal Delete -->
    <div class="modal" tabindex="-1" role="dialog" id="deleteModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">刪除設備分類</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="deleteForm">
                    <div class="modal-body">
                        {{ method_field('delete') }}
                        <h4>注意：刪除後資料將不可逆，系統會刪除所有相關連的資料</h4>
                        <table style="width: 100%" id="deleteView">
                            <tr>
                                <td>設備分類</td>
                                <td>@{{ name }}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" onclick="action_delete()">刪除</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div style="padding: 20px;">
        <div id="status-bar">
            <div class="alert alert-success" role="alert" v-show="isSuccess">
                操作成功！
            </div>
            <div class="alert alert-danger" role="alert" v-show="isFailed">
                操作失敗！
            </div>
        </div>
        <button type="button" class="btn btn-default" onclick="modal_create()">新增</button>
        <table class="table" id="categoryTable">
            <thead class="thead-dark">
                <th style="text-align: center;font-weight:bold;">設備分類</th>
                <th style="text-align: center;font-weight:bold;">操作</th>
            </thead>
            <tbody>
                @foreach($result as $row)
                <tr>
                    <td style="text-align: center;">{{ $row->name }}</td>
                    <td style="text-align: center;">
                        <button type="button" class="btn btn-primary" onclick="modal_update({{ $row->id }})">修改</button>
                        <button type="button" class="btn btn-danger" onclick="modal_delete({{ $row->id }})">刪除</button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <script type="text/javascript">
        $('#categoryTable').DataTable();
        var data_id  = null;
        var status_bar = new Vue({
            el: '#status-bar',
            data:{
                isSuccess: false,
                isFailed: false
            }
        })
        var deleteView = new Vue({
            el: '#deleteView',
            data: {
                name: null
            }
        })
        $('#createForm').validate({
            submitHandler: function(){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                   url: '{{ url('admin/device.category') }}',
                   method: 'post',
                   data: $('#createForm').serialize(),
                   success: function(){
                       Vue.set(status_bar,'isSuccess',true);
                       Vue.set(status_bar,'isFailed',false);
                       setTimeout(function () {
                           location.reload();
                       },5000);
                       $('#createModal').modal('hide');
                   },error: function (){
                        Vue.set(status_bar,'isSuccess',false);
                        Vue.set(status_bar,'isFailed',true);
                        $('#createModal').modal('hide');
                    }
                })
            },roles:{
                name: 'required'
            }
        })
        $('#updateForm').validate({
            submitHandler: function(){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: '{{ url('admin/device.category') }}/'+data_id,
                    method: 'post',
                    data: $('#updateForm').serialize(),
                    success: function(){
                        Vue.set(status_bar,'isSuccess',true);
                        Vue.set(status_bar,'isFailed',false);
                        setTimeout(function () {
                            location.reload();
                        },5000);
                        $('#updateModal').modal('hide');
                    },
                    error: function(){
                        Vue.set(status_bar,'isSuccess',false);
                        Vue.set(status_bar,'isFailed',true);
                        $('#updateModal').modal('hide');
                    }
                })
            },roles:{
                name: 'required'
            }
        })
        function modal_create(){
            $('#createModal').modal('show');
        }
        function modal_update(id){
            data_id = id;
            $('#updateModal').modal('show');
            get_data();
        }
        function modal_delete(id){
            data_id = id;
            $('#deleteModal').modal('show');
            get_data();
        }
        function action_delete(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '{{ url('admin/device.category') }}/'+data_id,
                method: 'post',
                data: $('#deleteForm').serialize(),
                success: function(){
                    Vue.set(status_bar,'isSuccess',true);
                    Vue.set(status_bar,'isFailed',false);
                    setTimeout(function () {
                        location.reload();
                    },5000);
                    $('#deleteModal').modal('hide');
                }
            })
        }
        function get_data(){
            if(typeof data_id == 'number'){
                $.ajax({
                    url: '{{ url('admin/device.category') }}/'+data_id,
                    method: 'get',
                    success: function (data) {
                        $('input[name="name"]').val(data['name']);
                        Vue.set(deleteView,'name',data['name']);
                    }
                })
            }
        }
    </script>
@endsection