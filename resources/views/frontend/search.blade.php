@extends('frontend.template')
@section('title',$webdata->title)
@section('keywords',$webdata->keywords)
@section('description',$webdata->description)
@inject('RepairPresenter','App\Presenters\RepairPresenter')
@section('content')
    <!-- DataTable Plugin -->
    <link rel="stylesheet" href="{{ URL::asset('css/bs4.datatable/dataTables.bootstrap4.min.css') }}">
    <script type="text/javascript" src="{{ URL::asset('js/bs4.datatable/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/bs4.datatable/dataTables.bootstrap4.min.js') }}"></script>    <!-- Vue js -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-8">
                <div class="repairView">
                    <table style="width: 100%">
                        <tr>
                            <td width="20%">
                                <div class="form-group">
                                    <label for="category">設備分類</label>
                                    <select id="category" class="form-control" onchange="loading_device_name(this)">
                                        {{ $RepairPresenter->showCategory($category,$qrcode['category']) }}
                                    </select>
                                </div>
                            </td>
                            <td width="20%">
                                <div class="form-group">
                                    <label for="device_name">設備名稱</label>
                                    <select id="device_name" class="form-control"></select>
                                </div>
                            </td>
                            <td width="20%">
                                <div class="form-group">
                                    <label for="location">位置</label>
                                    <input type="text" id="location" value="" class="form-control">
                                </div>
                            </td>
                            <td width="20%">
                                <label for="status">狀態</label>
                                <select id="status" class="form-control">
                                    <option selected></option>
                                    @foreach($status as $row)
                                    <option value="{{ $row->name }}">{{ $row->name }}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                    </table>
                    <!-- List -->
                    <table class="table" id="repairTable">
                        <thead class="thead-dark">
                            <th style="text-align: center;font-weight:bold; display: none;">device_category_id</th>
                            <th style="text-align: center;font-weight:bold; display: none;">device_name_id</th>
                            <th style="text-align: center;font-weight:bold;">設備分類</th>
                            <th style="text-align: center;font-weight:bold;">設備名稱</th>
                            <th style="text-align: center;font-weight:bold;">地點</th>
                            <th style="text-align: center;font-weight:bold;">故障分類</th>
                            <th style="text-align: center;font-weight:bold;">故障敘述</th>
                            <th style="text-align: center;font-weight:bold;">處理狀態</th>
                            <th style="text-align: center;font-weight:bold;">處理者回報訊息</th>
                        </thead>
                        <tbody>
                            @foreach($result as $row)
                            <tr>
                                <td style="display: none; text-align: center;">{{ $row->device_category_id }}</td>
                                <td style="display: none; text-align: center;">{{ $row->device_name_id }}</td>
                                <td style="text-align: center;">{{ $row->device_category->name }}</td>
                                <td style="text-align: center;">{{ $row->device_name->name }}</td>
                                <td style="text-align: center;">{{ $row->location }}</td>
                                <td style="text-align: center;">{{ $row->fault_category->name }}</td>
                                <td style="text-align: center;">{{ $row->content }}</td>
                                <td style="text-align: center;">{{ $row->repair_status->name }}</td>
                                <td style="text-align: center;">{{ $row->description }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
            <div class="col-sm-2"></div>
        </div>
    </div>
    <script type="text/javascript">
        var table = $('#repairTable').DataTable();
        function loading_device_name(event){
            $.ajax({
                url: '{{ url('ajax/device.name') }}/'+event.value,
                method: 'get',
                success: function(data){
                    console.log("work");
                    $('#device_name').empty();
                    $('#device_name').append(new Option('','',true));
                    $.each(data,function (index,value) {
                        $('#device_name').append(new Option(value['name'],value['id']));
                    });
                },error: function(){
                    alert("系統載入資料時發生錯誤，請聯絡系統管理員！");
                }
            })
            table.columns(0)
                .search($('#category').val())
                .draw();

        }
        $('#device_name').change(function(){
            table.columns(1)
                .search($('#device_name').val())
                .draw();
        })
        $('#location').on('keyup',function(){
            table.columns(4)
                .search($('#location').val())
                .draw();
        })
        $('#status').change(function(){
            table.columns(7)
                .search($('#status').val())
                .draw();
        });
        {{ $RepairPresenter->load_qrcode_data($qrcode) }}
    </script>
@endsection