<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>@yield('title')</title>
    <meta name="keywords" content="@yield('keywords')">
    <meta name="description" content="@yield('description')">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <script src="{{ URL::asset('js/jquery/jquery-3.3.1.min.js') }}"></script>
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap4/bootstrap.min.css') }}">
    <script type="text/javascript" src="{{ URL::asset('js/bootstrap4/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery/validate/jquery.validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery/validate/localization/messages_zh_TW.min.js') }}"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--[if IE 9]>
    <link rel="stylesheet" href="{{ URL::asset("css/bootstrap4/bootstrap-ie9.css") }}">
    <script src="{{ URL::asset('js/bootstrap4/bootstrap-ie9.js') }}" type="text/javascript"></script>
    <![endif]-->
    <!--[if lte IE 8]>
    <script src="{{ URL::asset('js/jquery/jquery-1.8.0.min.js') }}" type="text/javascript"></script>
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap4/bootstrap-ie8.css') }}">
    <script src="{{ URL::asset('js/html5shiv/html5shiv.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('js/bootstrap4/bootstrap-ie8.js') }}" type="text/javascript"></script>
    <![endif]-->
    <link rel="stylesheet" href="{{ URL::asset('css/NotoSans.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/frontend.css') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
<!-- Nav Bar -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">@yield('title')</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="{{ url('/') }}">報修登記 <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('repair/list') }}">查詢報修</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('auth/overhaul') }}">檢修員登入</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('auth/admin') }}">管理者登入</a>
            </li>
            <!--
            <li class="nav-item">
                <a class="nav-link disabled" href="#">Disabled</a>
            </li>
            -->
        </ul>
    </div>
</nav>
@yield('content')
</body>
</html>