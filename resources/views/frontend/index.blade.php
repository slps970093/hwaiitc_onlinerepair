@extends('frontend.template')
@section('title',$webdata->title)
@section('keywords',$webdata->keywords)
@section('description',$webdata->description)
@section('content')
    @inject('RepairPresenter','App\Presenters\RepairPresenter')
    <script type="text/javascript" src="{{ URL::asset('js/vue/vue.min.js') }}"></script>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4"></div>
            <div class="col-sm-4">
                <div class="repairView">
                    {{ $RepairPresenter->showDataSet($dataset) }}
                    <form id="repairForm">
                        <div id="app">
                            <div class="alert alert-success" role="alert" v-show="isSuccess">
                                操作成功！
                            </div>
                            <div class="alert alert-danger" role="alert" v-show="isFailed">
                                操作失敗！
                            </div>
                        </div>
                        <input type="hidden" name="dataset" value="{{ (!empty($dataset->id))?$dataset->id: ''}}">
                        <div class="form-group">
                            <label for="category">設備分類</label>
                            <select name="category" class="form-control" onchange="loading_device_name(this)">
                                {{ $RepairPresenter->showCategory($category,$qrcode['category']) }}
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="device_name">設備名稱</label>
                            <select name="device_name" class="form-control" onchange="loading_fault(this)"></select>
                        </div>
                        <div class="form-group">
                            <label for="fault">故障分類</label>
                            <select name="fault" class="form-control"></select>
                        </div>
                        <div class="form-group">
                            <label for="location">地點</label>
                            <input type="text" name="location" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="content">故障敘述</label>
                            <textarea class="form-control" name="content" maxlength="25"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary btn-lg btn-block">送出報修</button>
                    </form>
                </div>
            </div>
            <div class="col-sm-4"></div>
        </div>
        <script type="text/javascript">
            var app = new Vue({
                el: '#app',
                data:{
                    isSuccess: false,
                    isFailed: false
                }
            })
            $('#repairForm').validate({
                submitHandler: function(){
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: '{{ url('repair') }}',
                        method: 'post',
                        data: $('#repairForm').serialize(),
                        success: function(){
                            Vue.set(app,'isSuccess',true);
                            Vue.set(app,'isFailed',false);
                        },
                        error: function(){
                            Vue.set(app,'isSuccess',false);
                            Vue.set(app,'isFailed',true);
                        }
                    })
                },
                rules:{
                    category: 'required',
                    device_name: 'required',
                    fault: 'required',
                    location: {
                        minlength: 2,
                        maxlength: 10,
                        required: true
                    },
                    dataset: 'required'
                }
            })
            function loading_device_name(event){
                $.ajax({
                    url: '{{ url('ajax/device.name') }}/'+event.value,
                    method: 'get',
                    success: function(data){
                        $('select[name="device_name"]').empty();
                        $('select[name="device_name"]').append(new Option('','',true));
                        $.each(data,function (index,value) {
                            $('select[name="device_name"]').append(new Option(value['name'],value['id']));
                        });
                    },error: function(){
                        alert("系統載入資料時發生錯誤，請聯絡系統管理員！");
                    }
                })
            }

            function loading_fault(event){
                $.ajax({
                    url: '{{ url('ajax/fault.category') }}/'+event.value,
                    method: 'get',
                    success: function(data){
                        $('select[name="fault"]').empty();
                        $.each(data,function (index,value) {
                            $('select[name="fault"]').append(new Option(value['name'],value['id']));
                        });
                    },error: function(){
                        alert("系統載入資料時發生錯誤，請聯絡系統管理員！");
                    }
                })
            }
            {{ $RepairPresenter->load_qrcode_data($qrcode) }}
        </script>
    </div>
@endsection
