@extends('coreui.overhaultemplate')

@section('title',$webdata->title.' - 設備名稱管理')

@section('content')
    <!-- Jquery Validate -->
    <script type="text/javascript" src="{{ URL::asset('js/jquery/validate/jquery.validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery/validate/localization/messages_zh_TW.min.js') }}"></script>
    <!-- Vue js -->
    <script type="text/javascript" src="{{ URL::asset('js/vue/vue.min.js') }}"></script>
    <!-- DataTable Plugin -->
    <link rel="stylesheet" href="{{ URL::asset('css/bs4.datatable/dataTables.bootstrap4.min.css') }}">
    <script type="text/javascript" src="{{ URL::asset('js/bs4.datatable/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/bs4.datatable/dataTables.bootstrap4.min.js') }}"></script>
    <div class="modal" tabindex="-1" role="dialog" id="modalupdate">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">更新報修資料</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="updateForm">
                    <div class="modal-body">
                        {{ method_field('patch') }}
                        <div id="repairShow">
                            <table style="width: 100%">
                                <tr>
                                    <td>設備分類</td>
                                    <td>@{{ category }}</td>
                                </tr>
                                <tr>
                                    <td>設備名稱</td>
                                    <td>@{{ device_name }}</td>
                                </tr>
                                <tr>
                                    <td>故障分類</td>
                                    <td>@{{ fault }}</td>
                                </tr>
                                <tr>
                                    <td>地點</td>
                                    <td>@{{ location }}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="form-group">
                            <label for="status">狀態</label>
                            <select name="status" class="form-control">
                                @foreach($status as $row)
                                    <option value="{{ $row->id }}">{{$row->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="description">回報</label>
                            <textarea name="description" class="form-control">
                            </textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save changes</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div style="padding: 2%;">
        <div id="status-bar">
            <div class="alert alert-success" role="alert" v-show="isSuccess">
                操作成功！
            </div>
            <div class="alert alert-danger" role="alert" v-show="isFailed">
                操作失敗！
            </div>
        </div>
        <!-- 即時搜尋 -->
        <table width="60%">
            <tr>
                <td width="25%">
                    <div class="form-group">
                        <label for="category">設備分類</label>
                        <select id="categorySearch" class="form-control" onchange="loading_name(this)">
                            <option selected></option>
                            @foreach($category as $row)
                                <option value="{{ $row->id }}" >{{ $row->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </td>
                <td width="25%">
                    <div class="form-group">
                        <label for="name">設備名稱</label>
                        <select id="nameSearch" class="form-control">
                            <option selected></option>
                        </select>
                    </div>
                </td>
                <td width="25%">
                    <div class="form-group">
                        <label for="location">地點</label>
                        <input type="text" id="locationSearch" class="form-control">
                    </div>
                </td>
                <td width="25%">
                    <div class="form-group">
                        <label for="status">狀態</label>
                        <select id="statusSearch" class="form-control">
                            <option selected></option>
                            @foreach($status as $row)
                                <option value="{{ $row->name }}" >{{ $row->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </td>
            </tr>
        </table>
        <table class="table" id="repairTable">
            <thead class="thead-dark">
                <th style="text-align: center;font-weight:bold; display: none;">設備分類編號</th>
                <th style="text-align: center;font-weight:bold; display: none;">設備名稱編號</th>
                <th style="text-align: center;font-weight:bold;">設備分類</th>
                <th style="text-align: center;font-weight:bold;">設備名稱</th>
                <th style="text-align: center;font-weight:bold;">地點</th>
                <th style="text-align: center;font-weight:bold;">故障敘述</th>
                <th style="text-align: center;font-weight:bold;">故障分類</th>
                <th style="text-align: center;font-weight:bold;">狀態</th>
                <th style="text-align: center;font-weight:bold;">處理情形</th>
                <th style="text-align: center;font-weight:bold;">建立時間</th>
                <th style="text-align: center;font-weight:bold;">修改時間</th>
                <th style="text-align: center;font-weight:bold;">操作</th>
            </thead>
            <tbody>
            @foreach($result as $row)
                <tr>
                    <td style="text-align: center; display: none;">{{ $row->device_category->id }}</td>
                    <td style="text-align: center; display: none;">{{ $row->device_name->id }}</td>
                    <td style="text-align: center;">{{ $row->device_category->name }}</td>
                    <td style="text-align: center;">{{ $row->device_name->name }}</td>
                    <td style="text-align: center;">{{ $row->location }}</td>
                    <td style="text-align: center;">{{ $row->content }}</td>
                    <td style="text-align: center;">{{ $row->fault_category->name }}</td>
                    <td style="text-align: center;">{{ $row->repair_status->name }}</td>
                    <td style="text-align: center;">{{ $row->description }}</td>
                    <td style="text-align: center;">{{ $row->created_at->format('Y-m-d H:i:s') }}</td>
                    <td style="text-align: center;">{{ $row->updated_at->format('Y-m-d H:i:s') }}</td>
                    <td style="text-align: center;">
                        <button type="button" class="btn btn-primary" onclick="modal_update({{ $row->id }})">修改</button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <script type="text/javascript">
        var data_id = null;
        var table = $('#repairTable').DataTable();
        var repairShow = new Vue({
            el: '#repairShow',
            data: {
                category: null,
                device_name: null,
                fault: null,
                location: null
            }
        });
        var status_bar = new Vue({
            el: '#status-bar',
            data:{
                isSuccess: false,
                isFailed: false
            }
        })
        $('#statusSearch').change(function(){
            table.columns(7)
                .search($('#statusSearch').val())
                .draw();
        })
        $('#locationSearch').keyup(function(){
            table.columns(4)
                .search($('#locationSearch').val())
                .draw();
        })
        $('#nameSearch').change(function(){
            table.columns(1)
                .search($('#nameSearch').val())
                .draw();
        })
        $('#updateForm').validate({
            submitHandler: function(){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: '{{ url('overhaul/device.repair') }}/'+data_id,
                    method: 'post',
                    data: $('#updateForm').serialize(),
                    success: function (){
                        $('#modalupdate').modal('hide');
                        Vue.set(status_bar,'isSuccess',true);
                        Vue.set(status_bar,'isFailed',false);
                        setTimeout(function(){
                            location.reload();
                        },5000);
                    },error: function () {
                        $('#modalupdate').modal('hide');
                        Vue.set(status_bar,'isSuccess',false);
                        Vue.set(status_bar,'isFailed',true);
                    }
                })
            },rules:{
                status: 'required'
            }
        });
        function modal_update(id){
            data_id = id;
            $('#modalupdate').modal('show');
            $.ajax({
                url: '{{ url('overhaul/device.repair') }}/'+data_id,
                method: 'get',
                success: function(data){
                    Vue.set(repairShow,'category',data['category']);
                    Vue.set(repairShow,'device_name',data['device_name']);
                    Vue.set(repairShow,'fault',data['fault']);
                    Vue.set(repairShow,'location',data['location']);
                    $('select[name="status"]').val(data['status']);
                    $('textarea[name="description"]').val(data['description']);
                }
            })
        }
        function loading_name(event,search = 0){
            $.ajax({
                url: '{{ url('ajax/device.name') }}/'+event.value,
                success: function(data){
                    $('select[name="device_name"]').empty();
                    $('#nameSearch').empty();
                    $('#nameSearch').append(new Option('','',true));
                    $.each(data,function (index,value) {
                        $('select[name="device_name"]').append(new Option(value['name'], value['id']));
                        $('#nameSearch').append(new Option(value['name'], value['id']))
                    })
                }
            })
            if(search != 0){
                table.columns(1).search( event.value ).draw();
            }
        }



    </script>

@endsection