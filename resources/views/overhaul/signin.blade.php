<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>{{ $webdata->title }} - 檢修員登入</title>
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap4/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/NotoSans.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/signin.css') }}">
    <script type="text/javascript" src="{{ URL::asset('js/bootstrap4/bootstrap.min.js') }}"></script>
</head>
<body class="text-center">
@inject('LoginPresenter','App\Presenters\LoginPresenter')
    <form class="form-signin" action="{{ url('auth/overhaul') }}" method="post">
        {{ csrf_field() }}

        <img class="mb-4" src="{{ URL::asset('img/hwailogo.gif') }}" alt="" width="72" height="72">
            {{ $LoginPresenter->status($status) }}
        <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
        <label for="inputEmail" class="sr-only">account</label>
        <input type="text" id="inputEmail" name="username" class="form-control" placeholder="account" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
        <div class="checkbox mb-3">
            <label>
                <input type="checkbox" name="" value="remember-me"> Remember me
            </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
        <p class="mt-5 mb-3 text-muted">&copy; {{ date('Y') }}</p>
    </form>
</body>
</html>
