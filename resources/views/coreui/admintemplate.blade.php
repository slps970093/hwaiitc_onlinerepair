<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>@yield('title')</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Icons-->
        <link href="{{ URL::asset('node_modules/@coreui/icons/css/coreui-icons.min.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('node_modules/flag-icon-css/css/flag-icon.min.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('node_modules/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('node_modules/simple-line-icons/css/simple-line-icons.css') }}" rel="stylesheet">
        <!-- Main styles for this application-->
        <link href="{{ URL::asset('css/core-ui/style.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('vendors/pace-progress/css/pace.min.css') }}" rel="stylesheet">
        <!-- CoreUI and necessary plugins-->
        <script src="{{ URL::asset('node_modules/jquery/dist/jquery.min.js') }}"></script>
        <script src="{{ URL::asset('node_modules/popper.js/dist/umd/popper.min.js') }}"></script>
        <script src="{{ URL::asset('node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>
        <script src="{{ URL::asset('node_modules/pace-progress/pace.min.js') }}"></script>
        <script src="{{ URL::asset('node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js')}}"></script>
        <script src="{{ URL::asset('node_modules/@coreui/coreui/dist/js/coreui.min.js') }}"></script>
        <!-- Plugins and scripts required by this view-->
        <script src="{{ URL::asset('node_modules/chart.js/dist/Chart.min.js') }}"></script>
        <script src="{{ URL::asset('node_modules/@coreui/coreui-plugin-chartjs-custom-tooltips/dist/js/custom-tooltips.min.js') }}"></script>
        <script src="{{ URL::asset('js/core.ui/main.js') }}"></script>
        <link rel="stylesheet" href="{{ URL::asset('css/NotoSans.css') }}">
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
        @include('coreui.admin.header')
        <div class="app-body">
            @include('coreui.admin.sidebar')
            <main class="main">
                @yield('content')
            </main>
            @include('coreui.asidemenu')
        </div>
        @include('coreui.admin.footer')
    </body>
</html>