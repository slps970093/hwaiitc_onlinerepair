<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link" href="{{ url('admin/dashboard') }}">
                    <i class="nav-icon icon-speedometer"></i> Dashboard
                    <span class="badge badge-primary">NEW</span>
                </a>
            </li>
            <!-- 網站管理 -->
            <li class="nav-title">網站名稱</li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('admin/webinfo') }}">
                    <i class="nav-icon icon-drop"></i> 基本資料管理</a>
            </li>

            <!-- 檢修/管理者報修列表 -->
            <li class="nav-title">管理者/檢修者</li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('admin/admin') }}">
                    <i class="nav-icon icon-pencil"></i> 系統管理者</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('admin/overhaul') }}">
                    <i class="nav-icon icon-pencil"></i> 檢修員</a>
            </li>
            <!-- 校園報修列表 -->
            <li class="nav-title">校園設備</li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('admin/school.year') }}">
                    <i class="nav-icon icon-pencil"></i> 學年度</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('admin/device.category') }}">
                    <i class="nav-icon icon-pencil"></i> 設備分類</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('admin/device.name') }}">
                    <i class="nav-icon icon-pencil"></i> 設備名稱</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('admin/device.repair') }}">
                    <i class="nav-icon icon-pencil"></i> 報修資料</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('admin/repair.status') }}">
                    <i class="nav-icon icon-pencil"></i> 故障通報狀態</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('admin/fault.category') }}">
                    <i class="nav-icon icon-pencil"></i> 故障分類</a>
            </li>
            <!-- 行動條碼報修列表 -->
            <li class="nav-title">行動條碼報修</li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('admin/qrcode') }}">
                    <i class="nav-icon icon-pencil"></i> QR-CODE 行動報修</a>
            </li>
            <li class="nav-title">模組</li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="nav-icon icon-puzzle"></i> 設備報修模組</a>
                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('admin/module/hwai/drivelist') }}">
                            <i class="nav-icon icon-puzzle"></i> 校園財產登錄</a>
                    </li>
                    <!--
                    <li class="nav-item">
                        <a class="nav-link" href="base/cards.html">
                            <i class="nav-icon icon-puzzle"></i> 報修申報</a>
                    </li>
                    -->
                </ul>
            </li>

        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>