<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class device_repair extends Model
{
    //
    protected $table = 'device_repair';
    protected $fillable = [
        'school_year_id','device_category_id','device_name_id','fault_category_id','location','content','description','repair_status_id','description'
    ];
    public function repair_status(){
        return $this->belongsTo(RepairStatus::class,'repair_status_id');
    }
    public function device_category(){
        return $this->belongsTo(device_category::class,'device_category_id');
    }
    public function device_name(){
        return $this->belongsTo(device_name::class,'device_name_id');
    }
    public function fault_category(){
        return $this->belongsTo(fault_category::class,'device_category_id');
    }
    public function school_year(){
        return $this->belongsTo(school_year::class,'school_year_id');
    }
    
}
