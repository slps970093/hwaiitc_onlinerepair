<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class webinfo extends Model
{
    //
    protected $table = 'webinfo';
    protected $fillable = [
        'title','keyword','description'
    ];
    public $timestamps = false;
}
