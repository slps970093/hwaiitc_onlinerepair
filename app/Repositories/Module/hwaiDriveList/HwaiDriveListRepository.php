<?php
/**
 * Created by PhpStorm.
 * User: Chou_Yu_Hsien
 * Date: 2018/7/28
 * Time: 下午 05:27
 */

namespace App\Repositories\Module\hwaiDriveList;


use App\device_name;
use App\property_information;
use DB;

class HwaiDriveListRepository
{
    protected $property;
    protected $Device_name;
    public function __construct(property_information $property_information,device_name $device_name){
        $this->property = $property_information;
        $this->Device_name = $device_name;
    }

    public function get_data(){
        return $this->property->all();
    }
    public function get_where($id){
        return $this->property->find($id);
    }
    public function create($request){
         $result = $this->property->create(array(
            'serial' => htmlspecialchars($request->serial),
            'start' => (int) $request->start,
            'until' => (int) $request->until,
            'old_serial' => (empty($request->old_serial))? null : htmlspecialchars($request->old_serial),
            'device_category_id' => (int) $request->device_category,
            'device_name_id' => (int) $request->device_name,
            'name' => htmlspecialchars($request->name),
            'brand' => htmlspecialchars($request->brand),
            'model' => htmlspecialchars($request->model),
            'specification' => (empty($request->specification))? null : htmlspecialchars($request->specification),
            'qty' => (int) $request->qty,
            'unit' => (int) $request->unit,
            'original_location' => htmlspecialchars($request->original_location),
            'current_location' => htmlspecialchars($request->current_location),
            'year' => (int) $request->year,
            'custodian' => htmlspecialchars($request->custodian)
        ));
         if($result){
             return true;
         }
         return false;

    }
    public function update($id,$request){
        $result = $this->property->find($id);
        $result->serial = htmlspecialchars($request->serial);
        $result->start = (int) $request->start;
        $result->until = (int) $request->until;
        $result->old_serial = (empty($request->old_serial))? null : htmlspecialchars($request->old_serial);
        $result->device_category_id = (int) $request->device_category;
        $result->device_name_id = (int) $request->device_name;
        $result->name = htmlspecialchars($request->name);
        $result->brand = htmlspecialchars($request->brand);
        $result->model = htmlspecialchars($request->model);
        $result->specification = (empty($request->specification))? null : htmlspecialchars($request->specification);
        $result->qty = (int) $request->qty;
        $result->unit = (int) $request->unit;
        $result->original_location = htmlspecialchars($request->original_location);
        $result->current_location = htmlspecialchars($request->current_location);
        $result->year = (int) $request->year;
        $result->custodian = htmlspecialchars($request->custodian);
        $result->save();
        if($result){
            return true;
        }
        return false;
    }
    public function delete($id){
        $result = $this->property->find($id)->delete();
        if($result){
            return true;
        }
        return false;
    }
    public function import($result){
        try{
            DB::connection()->getPdo()->beginTransaction();
            foreach ($result as $row){
                $row['device_category_id'] = $this->Device_name->find($row['device_name_id'])->category->id;
                $this->property->create($row);
            }
            DB::connection()->getPdo()->commit();
        }catch (Exception $e){
            DB::connection()->getPdo()->rollBack();
            return false;
        }
        return true;
    }
    public function remove_device_category($id){
        $result = $this->property->where('device_category_id',$id)->delete();
        if($result){
            return true;
        }
        return false;
    }
    public function remove_device_name($id){
        $result = $this->property->where('device_name_id',$id)->delete();
        if($result){
            return true;
        }
        return false;
    }
}