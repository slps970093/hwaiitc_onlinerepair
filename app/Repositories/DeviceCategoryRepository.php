<?php
/**
 * Created by PhpStorm.
 * User: Chou_Yu_Hsien
 * Date: 2018/7/9
 * Time: 下午 08:35
 */

namespace App\Repositories;


use App\device_category;

class DeviceCategoryRepository
{
    protected $DeviceCategory;
    public function __construct(device_category $category){
        $this->DeviceCategory = $category;
    }
    public function get_data(){
        return $this->DeviceCategory->all();
    }
    public function create($request){
        $result = $this->DeviceCategory->create(array(
            'id' => null,
            'name' => htmlspecialchars($request->name)));
        if($result){
            return true;
        }
        return false;
    }
    public function update($id,$request){
        if(is_numeric($id)){
            $result = $this->DeviceCategory->find($id);
            $result->name = htmlspecialchars($request->name);
            $result->save();
            if($result){
                return true;
            }
        }
        return false;
    }
    public function delete($id){
        if(is_numeric($id)){
            $result = $this->DeviceCategory->find($id)->delete();
            if($result){
                return true;
            }
        }
        return false;
    }
    public function get_where($id){
        if(is_numeric($id)){
            return $this->DeviceCategory->find($id);
        }
        return false;
    }

}