<?php
/**
 * Created by PhpStorm.
 * User: Chou_Yu_Hsien
 * Date: 2018/8/9
 * Time: 下午 09:50
 */

namespace App\Repositories;


use App\Admin;

class AdminRepository
{
    private $Admin;
    public function __construct(Admin $admin)
    {
        $this->Admin = $admin;
    }
    public function get_data(){
        return $this->Admin->all();
    }
    public function get_where($id){
        if(is_numeric($id)){
            return $this->Admin->find($id);
        }
        return false;
    }
    public function create($request){
        $result = $this->Admin->create([
            'username' => htmlspecialchars($request->username),
            'password' => Hash::make($request->password),
            'name' => htmlspecialchars($request->name)
        ]);
        if($result){
            return true;
        }
        return false;
    }
    public function update($id,$request){
        $result = $this->Admin->find($id);
        $result->username = htmlspecialchars($request->username);
        $result->name = htmlspecialchars($request->name);
        $result->save();
        if($result){
            return true;
        }
        return false;
    }
    public function delete($id){
        if(is_numeric($id)){
            $result = $this->Admin->find($id)->delete();
            if($result){
                return true;
            }
        }
    }
    public function get_where_count($request){
        return $this->Admin->where('username',htmlspecialchars($request->username))->count();
    }
    public function get_username_data($request){
        return $this->Admin->where('username',htmlspecialchars($request->username))->first();
    }
    public function change_password($id,$request){
        $result = $this->Admin->find($id);
        $result->password = Hash::make($request->password);
        $result->save();
        if($result){
            return true;
        }
        return false;
    }

}