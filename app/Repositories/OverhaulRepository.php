<?php
/**
 * Created by PhpStorm.
 * User: Chou_Yu_Hsien
 * Date: 2018/7/8
 * Time: 下午 08:02
 */

namespace App\Repositories;

use App\Overhaul;
use Hash;

class OverhaulRepository
{
    protected  $overhaul;
    public function __construct(Overhaul $overhaul)
    {
        $this->overhaul = $overhaul;
    }
    public function get_data(){
        return $this->overhaul->all();
    }
    public function get_where($id){
        return $this->overhaul->find($id);
    }
    public function get_username_count($request){
        return $this->overhaul->where('username',htmlspecialchars($request->username))->count();
    }
    public function get_username_data($id){
        return $this->overhaul->find($id);
    }
    public function search_first_username($request){
        return $this->overhaul->where('username',htmlspecialchars($request->username))->first();
    }
    public function create($request){
        $result = $this->overhaul->create(array(
            'username' => htmlspecialchars($request->username),
            'password' => Hash::make($request->password),
            'name' => htmlspecialchars($request->name)
        ));
        if($request){
            return true;
        }
        return false;
    }
    public function update($id,$request){
        $result = $this->overhaul->find($id);
        $result->username = htmlspecialchars($request->username);
        $result->name = htmlspecialchars($request->name);
        $result->save();
        if($result){
            return true;
        }
        return false;
    }
    public function delete($id){
        $result = $this->overhaul->find($id)->delete();
        if($result){
            return true;
        }
        return false;
    }
    public function change_password($id,$request){
        $result = $this->overhaul->find($id);
        $result->password = Hash::make($request->password);
        $result->save();
        if($result){
            return true;
        }
        return false;
    }
}