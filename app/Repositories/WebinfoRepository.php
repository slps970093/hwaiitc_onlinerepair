<?php
/**
 * Created by PhpStorm.
 * User: Chou_Yu_Hsien
 * Date: 2018/8/5
 * Time: 上午 11:42
 */

namespace App\Repositories;


use App\webinfo;

class WebinfoRepository
{
    protected $webInfo;
    public function __construct(webinfo $webinfo)
    {
        $this->webInfo = $webinfo;
    }
    public function get_data(){
        return $this->webInfo->find(1);
    }
    public function update($request){
        $result = $this->webInfo->find(1);
        $result->title = htmlspecialchars($request->title);
        $result->keyword = (!empty($request->keyword))? htmlspecialchars($request->keyword): null;
        $result->description = (!empty($request->description))? htmlspecialchars($request->description) : null;
        $result->save();
        if($result){
            return true;
        }
        return false;
    }
}