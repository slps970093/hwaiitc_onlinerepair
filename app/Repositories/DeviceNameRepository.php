<?php
/**
 * Created by PhpStorm.
 * User: Chou_Yu_Hsien
 * Date: 2018/7/15
 * Time: 下午 08:48
 */

namespace App\Repositories;


use App\device_name;

class DeviceNameRepository
{
    protected $Devicename;
    public function __construct(device_name $name){
        $this->Devicename = $name;
    }
    public function get_data(){
        return $this->Devicename->all();
    }
    public function get_where($id){
        return $this->Devicename->find($id);
    }
    public function create($request){
        $result = $this->Devicename->create(array(
            'name' => htmlspecialchars($request->name),
            'extends' => (int) $request->category
        ));
        if($result){
            return true;
        }
        return false;

    }
    public function update($id,$request){
        $result = $this->Devicename->find($id);
        $result->name = htmlspecialchars($request->name);
        $result->extends = (int) $request->category;
        $result->save();
        if($result){
            return true;
        }
        return false;
    }
    public function delete($id){
        $result = $this->Devicename->find($id)->delete();
        if($result){
            return true;
        }
        return false;
    }
    public function get_where_option($extends){
        return $this->Devicename->where('extends',(int) $extends)->get();
    }
    public function remove_category($id){
        $result = $this->Devicename->where('extends',$id)->delete();
        if($result){
            return true;
        }
        return false;
    }

}