<?php
/**
 * Created by PhpStorm.
 * User: Chou_Yu_Hsien
 * Date: 2018/7/8
 * Time: 下午 08:51
 */

namespace App\Repositories;
use App\school_year;

class SchoolyearsRepository
{
    protected $schoolyear;
    public function __construct(school_year $school_year){
        $this->schoolyear = $school_year;
    }
    public function get_data(){
        return $this->schoolyear->all();
    }
    public function get_where($id){
        return $this->schoolyear->find($id);
    }
    public function get_default_dataset(){
        return $this->schoolyear->where('isEnable',true)->first();
    }
    public function disable_Current_dataset(){
        if($this->schoolyear->where('isEnable',true)->count() == 0){
            return true;
        }
        $result = $this->schoolyear->where('isEnable',true)->first();
        $result->isEnable = false;
        $result->save();
        if($result){
            return true;
        }
        return false;
    }
    public function find_dataset_count($request){
        return $this->schoolyear->where('name',$request->name)->count();
    }
    public function find_dataset_data($request){
        return $this->schoolyear->where('name',$request->name)->first();
    }
    public function create($request){
        $result = $this->schoolyear->create(array(
            'name' => htmlspecialchars($request->name),
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'isEnable' => (int) $request->isEnable
        ));
        if($result){
            return true;
        }
        return false;
    }
    public function update($id,$request){
        $result = $this->schoolyear->find($id);
        $result->name = htmlspecialchars($request->name);
        $result->start_date = $request->start_date;
        $result->end_date = $request->end_date;
        $result->isEnable = (int) $request->isEnable;
        $result->save();
        if($result){
            return true;
        }
        return false;
    }
    public function delete($id){
        $result = $this->schoolyear->find($id)->delete();
        if($result){
            return true;
        }
        return false;
    }

}