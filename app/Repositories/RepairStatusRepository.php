<?php
/**
 * Created by PhpStorm.
 * User: Chou_Yu_Hsien
 * Date: 2018/7/16
 * Time: 下午 04:53
 */

namespace App\Repositories;


use App\RepairStatus;

class RepairStatusRepository
{
    protected $RepairStatus;
    public function __construct(RepairStatus $repairStstus)
    {
        $this->RepairStatus = $repairStstus;
    }
    public function get_data(){
        return $this->RepairStatus->all();
    }
    public function get_where($id){
        return $this->RepairStatus->find($id);
    }
    public function create($request){
        $result = $this->RepairStatus->create(array(
            'id'=> null,
            'name' => htmlspecialchars($request->name)
        ));
        if($result){
            return true;
        }
        return false;
    }
    public function update($id,$request){
        $result = $this->RepairStatus->find($id);
        $result->name =  htmlspecialchars($request->name);
        $result->save();
        if($result){
            return true;
        }
        return false;
    }
    public function delete($id){
        $result = $this->RepairStatus->find($id)->delete();
        if($result){
            return true;
        }
        return false;
    }
}