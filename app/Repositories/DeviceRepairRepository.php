<?php
/**
 * Created by PhpStorm.
 * User: Chou_Yu_Hsien
 * Date: 2018/7/17
 * Time: 下午 02:25
 */

namespace App\Repositories;


use App\device_repair;

class DeviceRepairRepository
{
    protected $repair;
    public function __construct(device_repair $repair){
        $this->repair = $repair;
    }
    public function get_data($year = null){
        if(is_numeric($year)){
            return $this->repair->where('school_year_id',$year)->get();
        }
        return $this->repair->orderBy('id','desc')->get();
    }
    public function get_where($id){
        return $this->repair->find($id);
    }
    public function overhaul_update($id,$request){
        $result = $this->repair->find($id);
        $result->repair_status_id = (int) $request->status;
        $result->description = htmlspecialchars($request->description);
        $result->save();
        if($result){
            return true;
        }
        return false;
    }
    public function admin_update($id,$request){
        $result = $this->repair->find($id);
        $result->school_year_id = (int) $request->year_id;
        $result->device_category_id = (int) $request->category;
        $result->device_name_id = (int) $request->name;
        $result->fault_category_id = (int) $request->fault;
        $result->location = htmlspecialchars($request->location);
        $result->content = htmlspecialchars($request->content);
        $result->repair_status_id = (int) $request->status;
        $result->description = htmlspecialchars($request->description);
        $result->save();
        if($result){
            return true;
        }
        return false;
    }
    public function delete($id){
        $result = $this->repair->find($id)->delete();
        if($result){
            return true;
        }
        return false;
    }
    public function remove_category($id){
        if(is_numeric($id)){
            $result = $this->repair->where('device_category_id',$id)->delete();
            if($result){
                return true;
            }
        }
        return false;
    }
    public function remove_dataset($id){
        if(is_numeric($id)){
            $result = $this->repair->where('school_year_id',$id)->delete();
            if($result){
                return true;
            }
        }
        return false;
    }
    public function remove_device_name($id){
        if(is_numeric($id)){
            $result = $this->repair->where('device_name_id',$id)->delete();
            if($result){
                return true;
            }
        }
        return false;
    }
    public function get_dataset_data($id){
        return $this->repair->where('school_year_id',$id)->orderBy('id','desc')->get();
    }
    public function create($request){
        $result = $this->repair->create([
            'school_year_id' => (int)$request->dataset,
            'device_category_id' => (int)$request->category,
            'device_name_id' => (int)$request->device_name,
            'fault_category_id' => (int)$request->fault,
            'location' => htmlspecialchars($request->location),
            'content' => htmlspecialchars($request->content)
        ]);
        if($result){
            return true;
        }
        return false;
    }

    public function remove_fault($id){
        if(is_numeric($id)){
            $result = $this->repair->where('fault_category_id',$id)->delete();
            if($result){
                return true;
            }
        }
        return false;

    }
}