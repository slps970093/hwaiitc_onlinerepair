<?php
/**
 * Created by PhpStorm.
 * User: Chou_Yu_Hsien
 * Date: 2018/7/16
 * Time: 上午 11:48
 */

namespace App\Repositories;


use App\fault_category;

class FaultCategoryRepository
{
    protected $FaultCategory;
    public function __construct(fault_category $category){
        $this->FaultCategory = $category;
    }
    public function get_data(){
        return $this->FaultCategory->all();
    }
    public function get_where($id){
        return $this->FaultCategory->find($id);
    }
    public function create($request){
        $result=  $this->FaultCategory->create(array(
            'name' => htmlspecialchars($request->name),
            'extends' => (int) $request->extends
        ));
        if($result){
            return true;
        }
        return false;
    }
    public function update($id,$request){
        $result = $this->FaultCategory->find($id);
        $result->name = htmlspecialchars($request->name);
        $result->extends = (int) $request->extends;
        $result->save();
        if($result){
            return true;
        }
        return false;
    }
    public function delete($id){
        $result = $this->FaultCategory->find($id)->delete();
        if($result){
            return true;
        }
        return false;
    }
    public function get_select_option($extends){
        return $this->FaultCategory->where('extends',(int)$extends)->get();
    }
    public function remove_device_name($id){
        if(is_numeric($id)){
            $result = $this->FaultCategory->where('extends',$id)->delete();
            if($result){
                return true;
            }
        }
        return false;
    }
}