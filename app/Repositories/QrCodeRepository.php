<?php
/**
 * Created by PhpStorm.
 * User: Chou_Yu_Hsien
 * Date: 2018/7/19
 * Time: 下午 10:27
 */

namespace App\Repositories;


use App\qrcode;

class QrCodeRepository
{
    protected $Qrcode;
    public function __construct(qrcode $qrcode)
    {
        $this->Qrcode = $qrcode;
    }
    public function create($request,$repair_filename,$repair_search_filename){
        if(!empty($repair_filename) && !empty($repair_search_filename)){
            $result = $this->Qrcode->create(array(
                'location' => htmlspecialchars($request->location),
                'device_category_id' => (int) $request->device_category,
                'device_name_id' => (int) $request->device_name,
                'repair_url' => url('qrcode')."?category=".(int) $request->category."&device_name=".(int) $request->device_name."&location=".htmlspecialchars($request->location),
                'repair_search_url' => url('repair/list')."?location=".htmlspecialchars($request->location)."&category=".(int) $request->category."&device_name=".(int) $request->device_name,
                'repair_filename' => $repair_filename,
                'repair_search_filename' => $repair_search_filename
            ));
            if($result){
                return true;
            }
        }
        return false;
    }
    public function update($id,$request,$repair_filename,$repair_search_filename){
        if(!empty($repair_filename) && !empty($repair_search_filename)){
            $result  = $this->Qrcode->find((int) $id);
            $result->location = htmlspecialchars($request->location);
            $result->device_category_id = (int) $request->device_category;
            $result->device_name_id = (int) $request->device_name;
            $result->repair_url = url('qrcode')."?category=".(int) $request->category."&device_name=".(int) $request->device_name."&location=".htmlspecialchars($request->location);
            $result->repair_search_url = url('repair/list')."?location=".htmlspecialchars($request->location)."&category=".(int) $request->category."&device_name=".(int) $request->device_name;
            $result->repair_filename = $repair_filename;
            $result->repair_search_filename = $repair_search_filename;
            $result->save();
            if($result){
                return true;
            }
        }
        return false;
    }
    public function delete($id){
        $result = $this->Qrcode->find($id)->delete();
        if($result){
            return true;
        }
        return false;
    }
    public function get_where($id){
        return $this->Qrcode->find($id);
    }
    public function get_data(){
        return $this->Qrcode->all();
    }
    public function get_whereIn_data($id){
        if(is_array($id)){
            return $this->Qrcode->whereIn('id',$id)->get();
        }
        return false;
    }
    public function remove_category($id){
        $result = $this->Qrcode->where('device_category_id',$id)->delete();
        if($result){
            return true;
        }
    }
    public function remove_device_name($id){
        $result = $this->Qrcode->where('device_name_id',$id)->delete();
        if($result){
            return true;
        }
    }

}