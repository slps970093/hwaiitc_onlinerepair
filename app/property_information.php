<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class property_information extends Model
{
    //
    protected $table = 'property_information';
    protected $fillable = [
        'serial','start','until','old_serial',
        'device_category_id','device_name_id','name',
        'brand','model','specification','qty',
        'unit','original_location','current_location','year','custodian'
    ];
    public function device_category(){
        return $this->belongsTo(device_category::class,'device_category_id');
    }
    public function device_name(){
        return $this->belongsTo(device_name::class,'device_name_id');
    }
}
