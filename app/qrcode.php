<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class qrcode extends Model
{
    //
    protected $table = 'qrcode';
    protected $fillable = [
        'location','repair_url','repair_search_url','repair_filename','repair_search_filename','device_category_id','device_name_id'
    ];
    public $timestamps = false;
    public function device_name(){
        return $this->belongsTo(device_name::class,'device_name_id');
    }
    public function device_category(){
        return $this->belongsTo(device_category::class,'device_category_id');
    }
}
