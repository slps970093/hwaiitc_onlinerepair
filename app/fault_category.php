<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class fault_category extends Model
{
    //
    protected $table = 'fault_category';
    protected $fillable = [
        'extends','name'
    ];
    public $timestamps = false;
    public function device_name(){
       return $this->belongsTo(device_name::class,'extends');
    }

}
