<?php

namespace App\Http\Controllers;

use App\Http\Requests\RepairRequest;
use App\Repositories\DeviceCategoryRepository;
use App\Repositories\DeviceRepairRepository;
use App\Repositories\RepairStatusRepository;
use App\Repositories\SchoolyearsRepository;
use App\Repositories\WebinfoRepository;
use Illuminate\Http\Request;


class RepairController extends Controller
{
    protected $dataset;
    protected $category;
    protected $repair;
    protected $webinfo;
    protected $status;
    public function __construct(SchoolyearsRepository $schoolyearsRepository,DeviceCategoryRepository $categoryRepository,DeviceRepairRepository $repairRepository,
                                WebinfoRepository $webinfoRepository,RepairStatusRepository $repairStatusRepository)
    {
        $this->dataset = $schoolyearsRepository;
        $this->category = $categoryRepository;
        $this->repair = $repairRepository;
        $this->webinfo = $webinfoRepository;
        $this->status =  $repairStatusRepository;
    }

    public function index(Request $request){
        $QrCode = $this->getQRcodeInfo($request);
        $dataSet = $this->dataset->get_default_dataset();
        $category = $this->category->get_data();
        return view('frontend.index',[
            'qrcode' => $QrCode,
            'dataset' => $dataSet,
            'category' => $category,
            'webdata' => $this->webinfo->get_data()
        ]);
    }
    public function search(Request $request){
        $QrCode = $this->getQRcodeInfo($request);
        $dataSet = $this->dataset->get_default_dataset();
        $category = $this->category->get_data();
        $result = $this->repair->get_dataset_data($dataSet->id);
        return view('frontend.search',[
            'category' => $category,
            'qrcode' => $QrCode,
            'result' => $result,
            'webdata' => $this->webinfo->get_data(),
            'status' =>$this->status->get_data()
        ]);
    }
    public function sendrepair(RepairRequest $request){
        $result = $this->repair->create($request);
        if($result){
            return response()->json(array(
                'status' => true
            ),201);
        }
        return response()->json(array(
            'status' => false
        ),400);
    }
    private function getQRcodeInfo($request){
        // 載入Qr-Code
        $QrCodeData['category'] = (!empty($request->get('category')) || is_numeric($request->get('category')))? $request->get('category') : null;
        $QrCodeData['device_name'] = (!empty($request->get('device_name')) || is_numeric($request->get('device_name')))? $request->get('device_name') : null;
        $QrCodeData['location'] = (!empty($request->get('location')) || is_numeric($request->get('location')))? $request->get('location') : null;
        return $QrCodeData;
    }
}
