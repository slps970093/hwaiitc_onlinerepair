<?php

namespace App\Http\Controllers\Ajax;

use App\Repositories\DeviceNameRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DeviceNameController extends Controller
{
    protected  $device_name;
    public function __construct(DeviceNameRepository $nameRepository)
    {
        $this->device_name = $nameRepository;
    }
    public function get_option($id){
        $result = $this->device_name->get_where_option($id);
        $arr = array();
        $index=0;
        foreach ($result as $row){
            $arr[$index]['id'] = $row->id;
            $arr[$index]['name'] = $row->name;
            $index++;
        }
        return response()->json($arr,200);
    }
}
