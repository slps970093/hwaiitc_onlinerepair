<?php

namespace App\Http\Controllers\Ajax;

use App\Repositories\FaultCategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FaultCategoryController extends Controller
{
    //
    protected $FaultCategory;
    public function __construct(FaultCategoryRepository $faultCategoryRepository){
        $this->FaultCategory = $faultCategoryRepository;
    }
    public function get_option($id){
        $result = $this->FaultCategory->get_select_option($id);
        $index = 0;
        $arr = array();
        foreach ($result as $row) {
            $arr[$index]['id'] = $row->id;
            $arr[$index]['name'] = $row->name;
            $index++;
        }
        return response()->json($arr,200);
    }
}
