<?php

namespace App\Http\Controllers\Overhaul;

use App\Repositories\WebinfoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    private $webinfo;
    public function __construct(WebinfoRepository $webinfoRepository)
    {
        $this->webinfo = $webinfoRepository;
    }

    //
    public function index(){
        return view('overhaul.dashboard.index',['webdata' => $this->webinfo->get_data()]);
    }
}
