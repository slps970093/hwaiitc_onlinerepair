<?php

namespace App\Http\Controllers\Overhaul;

use App\Repositories\WebinfoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class SigninController extends Controller
{
    private $Webdata;
    public function __construct(WebinfoRepository $webinfoRepository)
    {
        $this->Webdata = $webinfoRepository;
    }
    public function index(Request $request){
        return view('overhaul.signin',['webdata' => $this->Webdata->get_data(),'status' => $request->get('status')]);
    }
    public function login(Request $request){
        $authRequest = $request->only(['username','password']);
        if(Auth::guard('overhaul')->attempt($authRequest,$request->has('remember'))){
            return redirect()->to(url('overhaul/dashboard'));
        }
        return redirect()->to(url('auth/overhaul')."?status=1");
    }
    public function logout(){
        Auth::guard('overhaul')->logout();
        return redirect()->to(url('auth/overhaul')."?status=2");
    }
}
