<?php

namespace App\Http\Controllers\Overhaul;

use App\Http\Controllers\Admin\SchoolyearController;
use App\Repositories\DeviceCategoryRepository;
use App\Repositories\DeviceNameRepository;
use App\Repositories\DeviceRepairRepository;
use App\Repositories\RepairStatusRepository;
use App\Repositories\SchoolyearsRepository;
use App\Repositories\WebinfoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DeviceRepairController extends Controller
{
    //
    protected $repair;
    protected $category;
    protected $device_name;
    protected $status;
    protected $webinfo;
    protected $schoolyear;
    public function __construct(DeviceRepairRepository $repairRepository,RepairStatusRepository $repairStatusRepository,DeviceNameRepository $nameRepository,
                                DeviceCategoryRepository $categoryRepository,WebinfoRepository $webinfoRepository,SchoolyearsRepository $schoolyearsRepository)
    {
        $this->repair = $repairRepository;
        $this->category = $categoryRepository;
        $this->device_name = $nameRepository;
        $this->status = $repairStatusRepository;
        $this->webinfo = $webinfoRepository;
        $this->schoolyear = $schoolyearsRepository;
    }
    public function index(){
        $repair = $this->repair->get_data($this->schoolyear->get_default_dataset()['id']);
        $category = $this->category->get_data();
        $device_name = $this->device_name->get_data();
        $status = $this->status->get_data();
        $webdata = $this->webinfo->get_data();
        return view('overhaul.device_repair.index',[
            'result' => $repair,
            'category' => $category,
            'device_name' => $device_name,
            'status' => $status,
            'webdata' => $webdata
        ]);
    }
    public function get_where($id)
    {
        $result = $this->repair->get_where($id);
        return response()->json(array(
            'category' => $result->device_category->name,
            'device_name' => $result->device_name->name,
            'fault' => $result->fault_category->name,
            'location' => $result->location,
            'status' => $result->repair_status_id,
            'description' => $result->description
        ),201);
    }
    public function update($id,Request $request){
        $result = $this->repair->overhaul_update($id,$request);
        if($result){
            return response()->json(array('status' => true),201);
        }
        return response()->json(array('status' => false),400);
    }




}
