<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\overhaul\overhaulCreateRequest;
use App\Http\Requests\Admin\overhaul\overhaulUpdateRequest;
use App\Overhaul;
use App\Repositories\WebinfoRepository;
use Validator;
use Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\OverhaulRepository;

class OverhaulController extends Controller
{
    protected  $overhaulRepository,$webinfo;
    public function __construct(OverhaulRepository $overhaulRepository,WebinfoRepository $webinfoRepository){
        $this->overhaulRepository = $overhaulRepository;
        $this->webinfo = $webinfoRepository;
    }

    public function index(){
        $result = $this->overhaulRepository->get_data();
        return view('admin.overhaul.index',[
            'result' => $result,
            'webdata' => $this->webinfo->get_data()
        ]);
    }
    public function create(overhaulCreateRequest $request){
        $count = $this->overhaulRepository->get_username_count($request);
        if($count != 0){
            return response()->json(array('status' => false,'message' => 'Overhaul already exists'),422);
        }
        $result = $this->overhaulRepository->create($request);
        if($result){
            return response()->json(array('status' => true),201);
        }
        return response()->json(array('status' => false),400);
    }
    public function update($id,overhaulUpdateRequest $request){
        $count = $this->overhaulRepository->get_username_count($request);
        if($count >= 1){
            $result = $this->overhaulRepository->search_first_username($request);
            if($result->id == $id){
                $result = $this->overhaulRepository->update($id,$request);
                if($result){
                    return response()->json(array('status' => true),201);
                }
            }else{
                return response()->json(array('status' => false,'message' => 'Overhaul already exists'),422);
            }
        }else{
            $result = $this->overhaulRepository->update($id,$request);
            if($result){
                return response()->json(array('status' => true),201);
            }
        }
        return response()->json(array('status' => false),400);
    }
    public function delete($id){
        $result  = $this->overhaulRepository->delete($id);
        if($result){
            return response()->json(array('status' => true),201);
        }
        return response()->json(array('status' => false),400);
    }
    public function get_data($id){
        $result = $this->overhaulRepository->get_where($id);
        return response()->json(array(
            'username' => htmlspecialchars($result->username),
            'name' => htmlspecialchars($result->name)
        ),200);
    }
    public function change_password($id,Request $request){
        $validator = Validator::make($request->all(),['password' => 'required|max:20|min:4'],['required' => '必須要有密碼','min' => '密碼最少4字元','max' => '密碼最多20字元']);
        if($validator->fails()){
            return response()->json($validator->error(),422);
        }else{
            $result = $this->overhaulRepository->change_password($id,$request);
            if($result){
                return response()->json(array('status' => true),201);
            }
        }
        return response()->json(array('status' => false),400);
    }
}
