<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\WebinfoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;

class SignController extends Controller
{
    protected $webinfo;
    public function __construct(WebinfoRepository $webinfoRepository)
    {
        $this->webinfo = $webinfoRepository;
    }

    public function index(Request $request){
        return view('admin.signin',['webdata' => $this->webinfo->get_data(),'status' => $request->get('status')]);
    }
    public function login(Request $request){
        $authRequest = $request->only(['username','password']);
        if(Auth::attempt($authRequest,$request->has('remember'))){
            return redirect()->to(url('admin/dashboard'));
        }
        return redirect()->to(url('auth/admin')."?status=1");

    }
    public function logout(){
        Auth::logout();
        return redirect()->to(url('auth/admin')."?status=2");
    }
}
