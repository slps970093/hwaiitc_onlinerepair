<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\admin\module\hwaiDriveListModuleController;
use App\Http\Requests\Admin\Devicename\NameCreateRequest;
use App\Repositories\DeviceCategoryRepository;
use App\Repositories\DeviceNameRepository;
use App\Repositories\DeviceRepairRepository;
use App\Repositories\FaultCategoryRepository;
use App\Repositories\Module\hwaiDriveList\HwaiDriveListRepository;
use App\Repositories\QrCodeRepository;
use App\Repositories\WebinfoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DevicenameContoller extends Controller
{
    protected $DeviceName = null;
    protected $Category = null;
    protected $Repair;
    protected $hwaiDriveList;
    protected $Qrcode;
    protected $fault;
    protected $webinfo;

    public function __construct(DeviceNameRepository $name,DeviceCategoryRepository $categoryRepository,DeviceRepairRepository $repairRepository,
                                QrCodeRepository $qrCodeRepository,HwaiDriveListRepository $hwaiDriveListRepository,FaultCategoryRepository $faultCategoryRepository,
                        WebinfoRepository $webinfoRepository){
        $this->Category = $categoryRepository;
        $this->DeviceName = $name;
        $this->Repair = $repairRepository;
        $this->Qrcode = $qrCodeRepository;
        $this->hwaiDriveList = $hwaiDriveListRepository;
        $this->fault = $faultCategoryRepository;
        $this->webinfo = $webinfoRepository;
    }
    public function index(){
        $result = $this->DeviceName->get_data();
        $category = $this->Category->get_data();
        return view('admin.device_name.index',[
            'category' => $category,
            'result' => $result,
            'webdata' => $this->webinfo->get_data()
        ]);
    }
    public function create(NameCreateRequest $request){
        $result = $this->DeviceName->create($request);
        if($result){
            return response()->json(array('status' => true),201);
        }
        return response()->json(array('status' => false),400);

    }
    public function update($id,NameCreateRequest $request){
        $result = $this->DeviceName->update($id,$request);
        if($result){
            return response()->json(array('status' => true),201);
        }
        return response()->json(array('status' => false),400);
    }
    public function delete($id){
        $result = $this->DeviceName->delete($id);
        if($result){
            return response()->json(array('status' => true),201);
        }
        return response()->json(array('status' => false),400);
    }
    public function get_where($id){
        $result = $this->DeviceName->get_where($id);
        return response()->json(array(
            'id' => $result->id,
            'name' => $result->name,
            'category_name' => $result->category->name,
            'extends' => $result->extends
        ),200);
    }
}
