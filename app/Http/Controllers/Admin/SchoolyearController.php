<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Schoolyears\SchoolyearsCreate;
use App\Http\Requests\Admin\Schoolyears\SchoolyearsUpdate;
use App\Repositories\DeviceRepairRepository;
use App\Repositories\WebinfoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\SchoolyearsRepository;
class SchoolyearController extends Controller
{
    //
    protected $schoolyearsRepository;
    protected $Repair;
    protected $webinfo;
    public function __construct(SchoolyearsRepository $schoolyearsRepository,DeviceRepairRepository $repairRepository,WebinfoRepository $webinfoRepository){
        $this->schoolyearsRepository = $schoolyearsRepository;
        $this->Repair = $repairRepository;
        $this->webinfo = $webinfoRepository;
    }
    public function index(){
        $result = $this->schoolyearsRepository->get_data();
        return view('admin.schoolyear.index',[
            'result' => $result,'webdata' => $this->webinfo->get_data()
        ]);
    }
    public function create(SchoolyearsCreate $request){
        $count = $this->schoolyearsRepository->find_dataset_count($request);
        if($count >= 1){
            return response()->json(array('status' => false,'message' => 'School year already exists'),422);
        }else{
            if($request->isEnable){
                $this->schoolyearsRepository->disable_Current_dataset();
            }
            $result = $this->schoolyearsRepository->create($request);
            if($result){
                return response()->json(array('status' => true),201);
            }
        }
        return response()->json(array('status' => false),400);
    }
    public function update($id,SchoolyearsUpdate $request){
        if($request->isEnable){
            $this->schoolyearsRepository->disable_Current_dataset();
        }
        $count = $this->schoolyearsRepository->find_dataset_count($request);
        if($count >= 1){
            $result = $this->schoolyearsRepository->find_dataset_data($request);
            if($result->id == $id){
                $result = $this->schoolyearsRepository->update($id,$request);
                if($result){
                    return response()->json(array('status' => true),201);
                }
            }
        }else{
            $result = $this->schoolyearsRepository->update($id,$request);
            if($result){
                return response()->json(array('status' => true),201);
            }
        }
        return response()->json(array('status' => false),400);
    }
    public function delete($id){
        $result = $this->schoolyearsRepository->delete($id);
        if($result){
            return response()->json(array('status' => true),201);
        }
        return response()->json(array('status' => false),400);
    }
    public function get_data($id){
        $result = $this->schoolyearsRepository->get_where($id);
        return response()->json(array(
            'name' => $result->name,
            'start_date' => $result->start_date,
            'end_date' => $result->end_date,
            'isEnable' => $result->isEnable
        ),200);
    }
}
