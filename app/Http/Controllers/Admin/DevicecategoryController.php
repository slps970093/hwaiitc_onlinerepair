<?php

namespace App\Http\Controllers\Admin;

use App\device_category;
use App\Http\Requests\Admin\Deviceicategory\CategoryRequest;
use App\Http\Requests\Admin\Deviceicategory\CategoryUpdateRequest;
use App\Repositories\DeviceCategoryRepository;
use App\Repositories\DeviceNameRepository;
use App\Repositories\DeviceRepairRepository;
use App\Repositories\Module\hwaiDriveList\HwaiDriveListRepository;
use App\Repositories\QrCodeRepository;
use App\Repositories\WebinfoRepository;
use App\Services\Qrcode\QrcodeDocumentService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DevicecategoryController extends Controller
{
    protected $Devicecategory;
    protected $Devicename;
    protected $DeviceRepair,$webinfo;
    protected $Qrcode;
    protected $hwaiDriveList;
    public function __construct(DeviceCategoryRepository $category,DeviceNameRepository $deviceNameRepository,
                            DeviceRepairRepository $repairRepository,QrCodeRepository $qrCodeRepository,
                            HwaiDriveListRepository $hwaiDriveListRepository,WebinfoRepository $webinfoRepository){
        $this->Devicecategory  = $category;
        $this->Devicename = $deviceNameRepository;
        $this->DeviceRepair = $repairRepository;
        $this->Qrcode = $qrCodeRepository;
        $this->hwaiDriveList = $hwaiDriveListRepository;
        $this->webinfo = $webinfoRepository;
    }

    public function index(){
        $result = $this->Devicecategory->get_data();
        return view('admin.device_category.index',[
            'result' => $result,
            'webdata' => $this->webinfo->get_data()
        ]);
    }
    public function create(CategoryRequest $request){
        $result = $this->Devicecategory->create($request);
        if($result){
            return response()->json(array('status' => true),200);
        }
        return response()->json(array('status' => false),400);
    }
    public function update($id,CategoryUpdateRequest $request){
        $result = $this->Devicecategory->update($id,$request);
        if($result){
            return response()->json(array('status' => true),200);
        }
        return response()->json(array('status' => false),400);
    }
    public function delete($id){
        $result = $this->Devicecategory->delete($id);
        if($result){
            return response()->json(array('status' => true),200);
        }
        return response()->json(array('status' => false),400);
    }
    public function get_data($id){
        $result = $this->Devicecategory->get_where($id);
        return response()->json(array(
            'id' => $result->id,
            'name' => $result->name
        ),200);
    }
}
