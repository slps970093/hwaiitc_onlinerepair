<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\DeviceRepair\RepairUpdateRequest;
use App\RepairStatus;
use App\Repositories\DeviceCategoryRepository;
use App\Repositories\DeviceNameRepository;
use App\Repositories\DeviceRepairRepository;
use App\Repositories\FaultCategoryRepository;
use App\Repositories\RepairStatusRepository;
use App\Repositories\SchoolyearsRepository;
use App\Repositories\WebinfoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DeviceRepairController extends Controller
{
    protected $repair;
    protected $school_year;
    protected $category;
    protected $device_name;
    protected $fault;
    protected $repair_status;
    protected $webinfo;
    public function __construct(DeviceCategoryRepository $categoryRepository,DeviceNameRepository $name,
                                FaultCategoryRepository $faultCategoryRepository,DeviceRepairRepository $repairRepository,
                                RepairStatusRepository $repairStatusRepository,SchoolyearsRepository $schoolyearsRepository,WebinfoRepository $webinfoRepository){
        $this->category = $categoryRepository;
        $this->school_year = $schoolyearsRepository;
        $this->device_name = $name;
        $this->fault = $faultCategoryRepository;
        $this->repair = $repairRepository;
        $this->repair_status = $repairStatusRepository;
        $this->webinfo = $webinfoRepository;
    }
    public function index(){
        $school_year = $this->school_year->get_data();
        $repair = $this->repair->get_data();
        $category = $this->category->get_data();
        $device_name = $this->device_name->get_data();
        $fault = $this->fault->get_data();
        $repair_status = $this->repair_status->get_data();
        return view('admin.device_repair.index',[
            'category' => $category,
            'device_name' => $device_name,
            'result' => $repair,
            'fault' => $fault,
            'repair_status' => $repair_status,
            'school_year' => $school_year,
            'webdata' => $this->webinfo->get_data()
        ]);
    }
    public function get_where($id){
        $result = $this->repair->get_where($id);
        return response()->json(array(
            'id' => $result->id,
            'school_year_id' => $result->school_year_id,
            'device_category_id' => $result->device_category_id,
            'device_name_id'=> $result->device_name_id,
            'fault_category_id'=>$result->fault_category_id,
            'location' => $result->location,
            'content' => $result->content,
            'repair_status_id' => $result->repair_status_id,
            'description'=> $result->description,
            'joinData' => array(
                'school_year' => $result->school_year->name,
                'category' => $result->device_category->name,
                'device_name' => $result->device_name->name,
                'fault' => $result->fault_category->name,
                'status' => $result->repair_status->name
            )
        ),200);
    }
    public function update($id,RepairUpdateRequest $request){
        $result = $this->repair->admin_update($id,$request);
        if($result){
            return response()->json(array(
                'status' => true
            ),200);
        }
        return response()->json(array(
            'status' => false
        ),400);
    }
    public function delete($id){
        $result = $this->repair->delete($id);
        if($result){
            return response()->json(array(
                'status' => true
            ),200);
        }
        return response()->json(array(
            'status' => false
        ),400);
    }
}
