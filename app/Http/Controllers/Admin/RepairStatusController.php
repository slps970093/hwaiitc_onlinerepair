<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\RepairStatus\StatusCreateRequest;
use App\Http\Requests\Admin\RepairStatus\StatusUpdateRequest;
use App\Repositories\RepairStatusRepository;
use App\Repositories\WebinfoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RepairStatusController extends Controller
{
    //
    protected $RepairStatus;
    protected $webinfo;
    public function __construct(RepairStatusRepository $repairStatusRepository,WebinfoRepository $webinfoRepository)
    {
        $this->RepairStatus = $repairStatusRepository;
        $this->webinfo = $webinfoRepository;
    }

    public function index(){
        $result = $this->RepairStatus->get_data();
        return view('admin.repair_status.index',[
            'result' => $result,'webdata' => $this->webinfo->get_data()
        ]);
    }
    public function create(StatusCreateRequest $request){
        $result = $this->RepairStatus->create($request);
        if($result){
            return response()->json(array('status' => true),201);
        }
        return response()->json(array('status' => false),400);
    }
    public function update($id,StatusUpdateRequest $request){
        $result = $this->RepairStatus->update($id,$request);
        if($result){
            return response()->json(array('status' => true),201);
        }
        return response()->json(array('status' => false),400);

    }
    public function delete($id){
        if($id <= 2){
            $result = $this->RepairStatus->delete($id);
            if($result){
                return response()->json(array('status' => true),201);
            }
        }
        return response()->json(array('status' => false),400);
    }
    public function get_where($id){
        $result = $this->RepairStatus->get_where($id);
        return response()->json(array(
            'name' => $result->name
        ),200);
    }
}
