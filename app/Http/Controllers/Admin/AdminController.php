<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Http\Requests\Admin\Admin\AdminCreateRequest;
use App\Http\Requests\Admin\Admin\AdminUpdateRequest;
use App\Repositories\AdminRepository;
use App\Repositories\WebinfoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Hash;

class AdminController extends Controller
{
    protected $webinfo;
    protected $Admin;
    public function __construct(WebinfoRepository $webinfoRepository,AdminRepository $adminRepository)
    {
        $this->webinfo = $webinfoRepository;
        $this->Admin = $adminRepository;
    }

    public function index(){
        $result = $this->Admin->get_data();
        return view('admin.admin.index',['result' => $result,'webdata' => $this->webinfo->get_data()]);
    }
    public function create(AdminCreateRequest $request){
        $count = $this->Admin->get_where_count($request);
        if($count == 0){
            $result = $this->Admin->create($request);
            if($result){
                return response()->json(array('status' => true),201);
            }
        }else{
            return response()->json(array('status' => false,'message' => 'Manager already exists'),422);
        }
        return response()->json(array('status' => false,'message' => 'database error!'),400);
    }
    public function update($id,AdminUpdateRequest $request){
        $count = $this->Admin->get_where_count($request);
        if($count == 0){
            $result = $this->Admin->update($id,$request);
            if($result){
                return response()->json(array('status' => true),201);
            }
        }else{
            $result = $this->Admin->get_username_data($request);
            if($result->id == $id){
                $result = $this->Admin->update($id,$request);
                if($result){
                    return response()->json(array('status' => true),201);
                }
            }else{
                return response()->json(array('status' => false,'message' => 'Manager already exists'),422);
            }
            return response()->json(array('status' => false,'message' => 'database error!'),400);
        }
    }
    public function change_password($id,Request $request){
        $validator = Validator::make($request->all(),['password' => 'required|max:20|min:4'],['required' => '必須要有密碼','min' => '密碼最少4字元','max' => '密碼最多20字元']);
        if($validator->fails()){
            return response()->json($validator->error(),422);
        }else{
            $result = $this->Admin->change_password($id,$request);
            if($result){
                return response()->json(array('status' => true),201);
            }
        }
        return response()->json(array('status' => false),400);
    }
    public function delete($id){
        if($id != 1){
            $result = $this->Admin->delete($id);
            if($result){
                return response()->json(array('status' => true),201);
            }
        }
        return response()->json(array('status' => false),400);
    }
    public function get_data($id){
        $result = $this->Admin->get_where($id);
        return response()->json(array(
            'name' => $result->name,
            'username' => $result->username
        ));
    }
}
