<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\WebinfoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    //
    protected $webinfo;
    public function __construct(WebinfoRepository $webinfoRepository)
    {
        $this->webinfo = $webinfoRepository;
    }

    public function index(){
        return view('admin.dashboard.index',['webdata' => $this->webinfo->get_data()]);
    }
}
