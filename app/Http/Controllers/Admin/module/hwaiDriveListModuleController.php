<?php

namespace App\Http\Controllers\admin\module;

use App\Http\Requests\Admin\Module\HwaiDriveFileRequest;
use App\Http\Requests\Admin\Module\HwaiDriveListRequest;
use App\Repositories\DeviceCategoryRepository;
use App\Repositories\DeviceNameRepository;
use App\Repositories\Module\hwaiDriveList\HwaiDriveListRepository;
use App\Repositories\WebinfoRepository;
use App\Services\Module\Hwai\Device\ImportHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class hwaiDriveListModuleController extends Controller
{
    protected $property;
    protected $device_category;
    protected $import;
    protected $device_name;
    protected $webinfo;
    public function __construct(HwaiDriveListRepository $driceListRepository, DeviceCategoryRepository $deviceCategoryRepository,
                                ImportHelper $importHelper,DeviceNameRepository $deviceNameRepository,WebinfoRepository $webinfoRepository)
    {
        $this->property = $driceListRepository;
        $this->device_category = $deviceCategoryRepository;
        $this->import = $importHelper;
        $this->device_name = $deviceNameRepository;
        $this->webinfo = $webinfoRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $result = $this->property->get_data();
        return view('admin.module.hwaiDeviceListModule.index',[
            'result' => $result,'webdata' => $this->webinfo->get_data()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $category = $this->device_category->get_data();
        return view('admin.module.hwaiDeviceListModule.create',['category' => $category,'webdata' => $this->webinfo->get_data()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HwaiDriveListRequest $request)
    {
        $result = $this->property->create($request);
        if($result){
            return redirect()->to(url('admin/module/hwai/drivelist'));
        }
        return response()->json(array('status' => false),400);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result  = $this->property->get_where($id);
        return view('admin.module.hwaiDeviceListModule.update',['category' => $this->device_category->get_data(),'result' => $result,'webdata' => $this->webinfo->get_data()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(HwaiDriveListRequest $request, $id)
    {
        //
        $result = $this->property->update($id,$request);
        if($result){
            return redirect()->to(url('admin/module/hwai/drivelist'));
        }
        return response()->json(array('status' => false),400);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $result = $this->property->delete($id);
        if($result){
            return response()->json(array('status' => true),201);
        }
        return response()->json(array('status' => false),400);
    }

    // 匯入Xlsx檔案
    public function import(HwaiDriveFileRequest $request){
        $data = $this->import->import($request->file('file'));
        $result = $this->property->import($data);
        if($result){
            return response()->json(array('status' => true),201);
        }
        return response()->json(array('status' => false),400);
    }
    // 匯出Xlsx檔案
    public function export(){
        $result = $this->import->export($this->property->get_data());
        return response()->download($result)->deleteFileAfterSend(true);
    }
    public function sample(){
        return response()->download(public_path('downloads/hwai/importDriveSample.xlsx'));
    }
    public function document_category(){
        return response()->download($this->import->category_export($this->device_name->get_data()))->deleteFileAfterSend(true);
    }
}
