<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\FaultCategory\FaultCategoryCreateRequest;
use App\Http\Requests\Admin\FaultCategory\FaultCategoryUpdateRequest;
use App\Repositories\DeviceCategoryRepository;
use App\Repositories\DeviceNameRepository;
use App\Repositories\DeviceRepairRepository;
use App\Repositories\FaultCategoryRepository;
use App\Repositories\WebinfoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FaultCategoryController extends Controller
{
    protected $FaultCategory;
    protected $DeviceName;
    protected $Repair;
    protected $webinfo;
    protected $category;
    public function __construct(FaultCategoryRepository $categoryRepository,DeviceNameRepository $nameRepository,DeviceRepairRepository $repairRepository,
                            WebinfoRepository $webinfoRepository,DeviceCategoryRepository $deviceCategoryRepository){
        $this->FaultCategory = $categoryRepository;
        $this->DeviceName = $nameRepository;
        $this->Repair = $repairRepository;
        $this->webinfo = $webinfoRepository;
        $this->category = $deviceCategoryRepository;
    }
    public function index(){
        $result = $this->FaultCategory->get_data();
        $device_name = $this->DeviceName->get_data();
        return view('admin.fault_category.index',[
            'result' => $result,'device_name' => $device_name,'webdata' => $this->webinfo->get_data(),'category' => $this->category->get_data()
        ]);
    }
    public function create(FaultCategoryCreateRequest $request){
        $result = $this->FaultCategory->create($request);
        if($result){
            return response()->json(array(
                'status' => true
            ),201);
        }
        return response()->json(array(
            'status' => false
        ),400);
    }
    public function update($id,FaultCategoryUpdateRequest $request){
        $result = $this->FaultCategory->update($id,$request);
        if($result){
            return response()->json(array(
                'status' => true
            ),201);
        }
        return response()->json(array(
            'status' => false
        ),400);
    }
    public function delete($id){
        $result = $this->FaultCategory->delete($id);
        if($result){
            return response()->json(array(
                'status' => true
            ),201);
        }
        return response()->json(array(
            'status' => false
        ),400);
    }
    public function get_where($id){
        $result = $this->FaultCategory->get_where($id);
        return response()->json(array(
            'name' => $result->name,
            'extends_name' => $result->extends,
            'device_name' => $result->device_name->name,
            'category' => $result->device_name->extends
        ));

    }
}
