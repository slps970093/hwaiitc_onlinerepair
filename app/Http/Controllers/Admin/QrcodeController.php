<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Qrcode\QrcodeCreateRequest;
use App\Http\Requests\Admin\Qrcode\QrcodeUpdateRequest;
use App\Repositories\DeviceCategoryRepository;
use App\Repositories\DeviceNameRepository;
use App\Repositories\QrCodeRepository;
use App\Repositories\WebinfoRepository;
use App\Services\Qrcode\QrcodeDocumentService;
use App\Services\Qrcode\QrcodeService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QrcodeController extends Controller
{
    //
    protected $QrcodeService;
    protected $DeviceCategory;
    protected $DeviceName;
    protected $Qrcode;
    protected  $DocumentService;
    protected $webinfo;
    public function __construct(QrcodeService $qrcodeService,DeviceNameRepository $deviceNameRepository,
                                DeviceCategoryRepository $deviceCategoryRepository,QrCodeRepository $qrCodeRepository,QrcodeDocumentService $documentService,
                                WebinfoRepository $webinfoRepository)
    {
        $this->QrcodeService = $qrcodeService;
        $this->Qrcode = $qrCodeRepository;
        $this->DeviceCategory = $deviceCategoryRepository;
        $this->webinfo = $webinfoRepository;
        $this->DeviceName = $deviceNameRepository;
        $this->DocumentService  = $documentService;
    }
    public function index(){
        $result = $this->Qrcode->get_data();

        return view('admin.qrcode.index',[
            'result' => $result,
            'category' => $this->DeviceCategory->get_data(),
            'webdata' => $this->webinfo->get_data()
        ]);
    }
    public function create(QrcodeCreateRequest $request){
        $data = $this->QrcodeService->create($request);
        if(is_array($data)){
            $result = $this->Qrcode->create($request,$data['repair_file'],$data['search_file']);
            if($result){
                return response()->json(['status' => true],201);
            }
        }
        return response()->json(['status' => false],400);
    }
    public function update($id,QrcodeUpdateRequest $request){
        $result = $this->Qrcode->get_where($id);
        $data = $this->QrcodeService->update($result,$request);
        if(is_array($data)){
            $result =  $this->Qrcode->update($id,$request,$data['repair_file'],$data['search_file']);
            if($result){
                return response()->json(['status' => true],201);
            }
        }
        return response()->json(['status' => false],400);
    }
    public function delete($id){
        $result = $this->QrcodeService->remove($this->Qrcode->get_where($id));
        if($result){
            if($this->Qrcode->delete($id)){
                return response()->json(['status' => true],201);
            }
        }
        return response()->json(['status' => false],400);
    }
    public function get_where($id){
        $result = $this->Qrcode->get_where($id);
        return response()->json(array(
            'device_category_id' => $result->device_category_id,
            'device_name_id' => $result->device_name_id,
            'location' => htmlspecialchars($result->location),
            'joinData' => [
                'device_category' => $result->device_name->name,
                'device_name' => $result->device_category->name
            ]
        ),200);
    }
    public function output_word(){
        $score = $this->DocumentService->create($this->Qrcode->get_data());
        return response()->download($score)->deleteFileAfterSend(true);
    }
    public function muitl_output_word(Request $request){
        if(is_array($request->id)){
            $score = $this->DocumentService->create($this->Qrcode->get_whereIn_data($request->id));
            return response()->download($score)->deleteFileAfterSend(true);
        }
        return response()->json(array('status' => false,'message' => 'field not found!'),400);
    }
}
