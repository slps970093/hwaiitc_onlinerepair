<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\WebinfoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WebinfoController extends Controller
{
    //
    protected $webinfo;
    public function __construct(WebinfoRepository $webinfoRepository)
    {
        $this->webinfo = $webinfoRepository;
    }

    public function index(){
        return view('admin.Webinfo.index',['webdata' => $this->webinfo->get_data()]);
    }
    public function update(Request $request){
        $result = $this->webinfo->update($request);
        if($result){
            return redirect()->to(url('admin/webinfo'));
        }
        return response()->json(array('status' => false),400);
    }
}
