<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RepairRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dataset' => 'required|integer',
            'category' =>  'required|integer',
            'device_name' => 'required|integer',
            'fault' =>  'required|integer',
            'location' => 'required|max:10|min:2',
        ];
    }
}
