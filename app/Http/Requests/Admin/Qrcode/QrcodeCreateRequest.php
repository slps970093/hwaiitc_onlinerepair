<?php

namespace App\Http\Requests\Admin\Qrcode;

use Illuminate\Foundation\Http\FormRequest;

class QrcodeCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'device_category' => 'required',
            'device_name' => 'required',
            'location' => 'required|min:2|max:10'
        ];
    }
}
