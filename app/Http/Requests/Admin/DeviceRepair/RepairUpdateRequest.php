<?php

namespace App\Http\Requests\Admin\DeviceRepair;

use Illuminate\Foundation\Http\FormRequest;

class RepairUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'year_id' => 'required',
            'category' => 'required',
            'device_name' => 'required',
            'location' => 'required|min:2|max:10',
            'fault' => 'required',
            'status' => 'required'
        ];
    }
}
