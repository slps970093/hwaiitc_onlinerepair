<?php

namespace App\Http\Requests\Admin\Module;

use Illuminate\Foundation\Http\FormRequest;

class HwaiDriveListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'serial' => 'required',
            'start' => 'required|integer',
            'start' => 'required|integer',
            'device_category' => 'required',
            'device_name' => 'required',
            'name' => 'required',
            'brand' =>'required',
            'model' => 'required',
            'qty' => 'required|integer',
            'unit' => 'required',
            'original_location' => 'required',
            'current_location' => 'required',
            'year' => 'required|integer',
            'custodian' => 'required'
        ];
    }
}
