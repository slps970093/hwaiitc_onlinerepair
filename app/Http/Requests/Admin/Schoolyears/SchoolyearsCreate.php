<?php

namespace App\Http\Requests\Admin\Schoolyears;

use Illuminate\Foundation\Http\FormRequest;

class SchoolyearsCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:10|min:2',
            'isEnable' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date'
        ];
    }
}
