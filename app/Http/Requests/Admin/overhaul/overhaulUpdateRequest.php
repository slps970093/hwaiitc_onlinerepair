<?php

namespace App\Http\Requests\Admin\overhaul;

use Illuminate\Foundation\Http\FormRequest;

class overhaulUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'username' => 'required|max:20|min:5',
            'name' =>'required|max:10|min:2'
        ];
    }
}
