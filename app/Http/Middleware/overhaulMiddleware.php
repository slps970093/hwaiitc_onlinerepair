<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class overhaulMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $responce = $next($request);
        if(!Auth::guard('overhaul')->check()){
            return redirect('auth/overhaul?status=2');
        }

        return $responce;
    }
}
