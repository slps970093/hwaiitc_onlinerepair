<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class device_category extends Model
{
    //
    protected $table = 'device_category';
    protected $fillable = [
        'name'
    ];
    public $timestamps = false;
}
