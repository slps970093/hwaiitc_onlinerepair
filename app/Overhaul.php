<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Overhaul extends Authenticatable
{
    //
    protected $table = 'Overhaul';
    protected $fillable = ['username','password','name','phone'];
    protected $hidden = [
        'password', 'remember_token',
    ];
}
