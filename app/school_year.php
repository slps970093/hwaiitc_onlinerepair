<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class school_year extends Model
{
    //
    protected $table = 'school_year';
    protected $fillable = [
        'name','start_date','end_date','isEnable'
    ];
}
