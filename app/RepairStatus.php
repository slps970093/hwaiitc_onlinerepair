<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RepairStatus extends Model
{
    //
    protected $table = 'status';
    protected $fillable = [
        'id','name'
    ];
    public $timestamps = false;
}
