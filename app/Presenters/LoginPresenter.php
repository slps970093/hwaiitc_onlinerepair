<?php
/**
 * Created by PhpStorm.
 * User: Chou_Yu_Hsien
 * Date: 2018/8/16
 * Time: 下午 09:48
 */

namespace App\Presenters;


class LoginPresenter
{
    public function status($code){
        if(!empty($code) && is_numeric($code)){
            switch ($code){
                case 1:
                    echo '<div class="alert alert-danger" role="alert">你輸入的帳號或是密碼錯誤，請重新輸入！</div>';
                    break;
                default:
                    echo '<div class="alert alert-info" role="alert">請先登入!</div>';
                    break;
            }
        }
    }
}