<?php
/**
 * Created by PhpStorm.
 * User: Chou_Yu_Hsien
 * Date: 2018/8/5
 * Time: 下午 06:44
 */

namespace App\Presenters;


class RepairPresenter
{
    public function showCategory($data,$default = null){
        if(is_null($default)){
            echo "<option selected></option>";
        }
        foreach ($data as $row){
            if(!is_null($default)){
                if($row->id == $default){
                    echo '<option value="'.$row->id.'" selected="selected">'.$row->name.'</option>';
                }else{
                    echo '<option value="'.$row->id.'">'.$row->name.'</option>';
                }
            }else{
                echo '<option value="'.$row->id.'">'.$row->name.'</option>';
            }
        }
    }
    public function showStatus($data,$default = null){
        foreach ($data as $row){
            if(!is_null($default)){
                if($row->id == $default){
                    echo '<option value="'.$row->id.'" selected="selected">'.$row->name.'</option>';
                }else{
                    echo '<option value="'.$row->id.'">'.$row->name.'</option>';
                }
            }else{
                echo '<option value="'.$row->id.'">'.$row->name.'</option>';
            }
        }
    }
    public function load_qrcode_data($data){
        if(!is_null($data['category'])){
            echo "qrcode_loading_device_name(".$data['category'].")";
            echo <<<HTML
            function qrcode_loading_device_name(id,defaultValue){
                $.ajax({
                    url: '{{ url('ajax/device.name') }}/'+id,
                    method: 'get',
                    success: function(){
                        $.each(data,function (index,value) {
                            if(defaultValue == value['id']){
                                $('select[name="device_name"]').append(new Option(value['name'],value['id']),true);
                            }else{
                                $('select[name="device_name"]').append(new Option(value['name'],value['id']));
                            }
                        });
                        if(typeof $('select[name="device_name"]')  !== 'undefined'){
                            qrcode_loading_fault($('select[name="device_name"]'));
                        }
                    },error: function(){
                        alert("系統載入資料時發生錯誤，請聯絡系統管理員！");
                    }
                })
            }
            function qrcode_loading_fault(id){
                $.ajax({
                    url: '{{ url('ajax/fault.category') }}/'+event.value,
                    method: 'get',
                    success: function(data){
                        $.each(data,function (index,value) {
                            if(defaultValue == value['id']){
                                $('select[name="fault"]').append(new Option(value['name'],value['id']),true);
                            }else{
                                $('select[name="fault"]').append(new Option(value['name'],value['id']));
                            }
                        });
                    },error: function(){
                        alert("系統載入資料時發生錯誤，請聯絡系統管理員！");
                    }
                })
            }
HTML;
        }
    }
    public function showDataSet($dataset){
        if(isset($dataset->id) && !empty($dataset->id)){
            echo '<div class="alert alert-info" role="alert">';
            echo "開放報修時間".$dataset->start_date." - ".$dataset->end_date;
            echo '</div>';
        }
    }
}