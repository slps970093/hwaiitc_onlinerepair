<?php
/**
 * Created by PhpStorm.
 * User: Chou_Yu_Hsien
 * Date: 2018/7/19
 * Time: 下午 10:27
 */

namespace App\Services\Qrcode;



use Illuminate\Support\Facades\Storage;
use SimpleSoftwareIO\QrCode\BaconQrCodeGenerator;

class QrcodeService
{
    private $Qrcode;
    public function __construct()
    {
        $this->Qrcode = new BaconQrCodeGenerator();
    }

    public function create($request){
        $repair_file = "repair-".str_random(10).".png";
        $search_file = "search-".str_random(10).".png";

        $this->Qrcode->format('png')->size(260)->generate(url('qrcode')."?category=".(int) $request->category."&device_name=".(int) $request->device_name."&location=".htmlspecialchars($request->location),public_path('qrcode/'.$repair_file));
        $this->Qrcode->format('png')->size(260)->generate(url('repair/list')."?location=".htmlspecialchars($request->location)."&category=".(int) $request->category."&device_name=".(int) $request->device_name,public_path('qrcode/'.$search_file));

        return array(
            'repair_file' => $repair_file,
            'search_file' => $search_file
        );
    }
    public function update($resultData,$request){
        // remove qrcode file
        Storage::disk('qrcode')->delete([$resultData->repair_filename,$resultData->repair_search_filename]);
        // create new qrcode
        $repair_file = "repair-".str_random(10).".png";
        $search_file = "search-".str_random(10).".png";
        $this->Qrcode->format('png')->size(260)->generate(url('qrcode')."?category=".(int) $request->category."&device_name=".(int) $request->device_name."&location=".htmlspecialchars($request->location),public_path('qrcode/'.$repair_file));
        $this->Qrcode->format('png')->size(260)->generate(url('repair/list')."?location=".htmlspecialchars($request->location)."&category=".(int) $request->category."&device_name=".(int) $request->device_name,public_path('qrcode/'.$search_file));
        return array(
            'repair_file' => $repair_file,
            'search_file' => $search_file
        );
    }
    public function remove($resultData){
        $result = Storage::disk('qrcode')->delete([$resultData->repair_filename,$resultData->repair_search_filename]);
        if($result){
            return true;
        }
        return false;
    }

}