<?php
/**
 * Created by PhpStorm.
 * User: Chou_Yu_Hsien
 * Date: 2018/7/21
 * Time: 下午 11:50
 */

namespace App\Services\Qrcode;

use PhpOffice\PhpWord\PhpWord;
class QrcodeDocumentService
{
    private $filename = 'output_qrcode';
    private $tableStyle = array(
        'borderColor' => '006699',
        'borderSize'  => 6,
        'cellMargin'  => 50
    );
    private  $FontStyle = array(
        'bold' => true,
        'size' => 20

    );
    private  $imageStyle = array(
        'height' => 180,
        'width' => 180,
        'align'=>'both'
    );

    public function create($source){
        if(file_exists(public_path('downloads/qrcode.docx'))){
            unlink(public_path('downloads/qrcode.docx'));
        }
        $phpWord = new PhpWord();
        $section = $phpWord->addSection();
        // create table
        $table = $section->addTable('qrCodeTable',$this->tableStyle,array('bgColor' => 'FFFFFF','align'=>'both'));
        $table->addRow();
        $Total = count($source->toArray())-1;
        $count = 0;
        foreach ($source as $row){
            $cell = $table->addCell();
            $txt = $row->location." ".$row->device_category->name." ".$row->device_name->name;
            $cell->addText($txt,$this->FontStyle,array('align'=>'both'));
            $cell->addTextBreak(1);
            $cell->addText("報修行動條碼",$this->FontStyle,array('align'=>'both'));
            $cell->addImage(url('qrcode')."/".$row->repair_filename,$this->imageStyle);
            $cell = $table->addCell();
            $txt = $row->location." ".$row->device_category->name." ".$row->device_name->name;
            $cell->addText($txt,$this->FontStyle,array('align'=>'both'));
            $cell->addTextBreak(1);
            $cell->addText("報修進度查詢行動條碼",$this->FontStyle,array('align'=>'both'));
            $cell->addImage(url('qrcode')."/".$row->repair_search_filename,$this->imageStyle);
            if($count != $Total){
                $table->addRow();
                $count++;
            }
        }
        // Saving the document as OOXML file...
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save(public_path('downloads/qrcode.docx'));
        return public_path('downloads/qrcode.docx');
    }

}