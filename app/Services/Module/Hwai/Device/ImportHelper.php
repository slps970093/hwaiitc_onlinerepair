<?php
/**
 * Created by PhpStorm.
 * User: Chou_Yu_Hsien
 * Date: 2018/8/2
 * Time: 下午 10:39
 */

namespace App\Services\Module\Hwai\Device;

use App\Repositories\DeviceNameRepository;
use App\Repositories\Module\hwaiDriveList\HwaiDriveListRepository;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;

class ImportHelper
{
    protected $property_information;
    protected $device_name;
    public function __construct(HwaiDriveListRepository $hwaiDriveListRepository,DeviceNameRepository $deviceNameRepository)
    {
        $this->property_information = $hwaiDriveListRepository;
        $this->device_name = $deviceNameRepository;
    }
    public function import($file){
        $reader = new Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($file);
        //取得第一張工作表
        $sheet = $spreadsheet->getSheet(0);
        $RowData = $sheet->getHighestRow(); //取得總行數
        $ColData = $sheet->getHighestColumn();  //取得總列數
        $data = [];
        // 表格欄位名稱
        $field = [
            'serial','start','until','old_serial',
            'device_name_id','name',
            'brand','model','specification','qty',
            'unit','original_location','current_location','year','custodian'
        ];
        $count = 0;
        for ($row = 2; $row <= $RowData; $row++){
            $tmp = 0;
            for($col = 'A'; $col <= $ColData; $col++){
                $data[$count][$field[$tmp]] = (!empty($sheet->getCell($col.$row)->getValue()))? htmlspecialchars( $sheet->getCell($col.$row)->getValue()) : null;
                $tmp++;
            }
            $tmp = 0;
            $count++;
        }
        return (is_array($data))? $data : false;
    }
    public function export($data){
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        // 產生第一列資料欄位
        $arr = array(
            'A1' => '財產序號','B1' => '起', 'C1' => '迄', 'D1' => '舊財編','E1'=>'設備分類(請填入代碼)',
            'F1' => '設備名稱','G1' => '廠牌', 'H1' => '型號','I1' => '規格', 'J1' => '數量', 'K1' => '單位',
            'L1' => '原始地點','M1' => '目前地點', 'N1' => '使用年限', 'O1' => '保管人'
        );
        foreach ($arr as $key => $value) {
            $sheet->setCellValue($key,$value);
        }
        $count = 2;
        //產生資料
        foreach ($data as $row){
            $sheet->setCellValue('A'.$count,$row->serial);
            $sheet->setCellValue('B'.$count,$row->start);
            $sheet->setCellValue('C'.$count,$row->until);
            $sheet->setCellValue('D'.$count,$row->old_serial);
            $sheet->setCellValue('E'.$count,$row->device_name_id);
            $sheet->setCellValue('F'.$count,$row->name);
            $sheet->setCellValue('G'.$count,$row->brand);
            $sheet->setCellValue('H'.$count,$row->model);
            $sheet->setCellValue('I'.$count,$row->specification);
            $sheet->setCellValue('J'.$count,$row->qty);
            $sheet->setCellValue('K'.$count,$row->unit);
            $sheet->setCellValue('L'.$count,$row->original_location);
            $sheet->setCellValue('M'.$count,$row->current_location);
            $sheet->setCellValue('N'.$count,$row->year);
            $sheet->setCellValue('O'.$count,$row->custodian);
            $count++;
        }
        $writer = IOFactory::createWriter($spreadsheet, "Xlsx");
        $writer->save(public_path('downloads/hwai/export-'.date('Y-m-d').'.xlsx'));
        return public_path('downloads/hwai/export-'.date('Y-m-d').'.xlsx');
    }

    public function category_export($data){
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        // 產生第一列資料欄位
        $arr = array(
            'A1' => '分類代碼','B1' => '設備分類', 'C1' => '設備名稱'
        );
        foreach ($arr as $key => $value) {
            $sheet->setCellValue($key,$value);
        }
        $count = 2;
        //產生資料
        foreach ($data as $row){
            $sheet->setCellValue('A'.$count,$row->id);
            $sheet->setCellValue('B'.$count,$row->category->name);
            $sheet->setCellValue('C'.$count,$row->name);
            $count++;
        }
        $writer = IOFactory::createWriter($spreadsheet, "Xlsx");
        $writer->save(public_path('downloads/hwai/category-'.date('Y-m-d').'.xlsx'));
        return public_path('downloads/hwai/category-'.date('Y-m-d').'.xlsx');
    }
}