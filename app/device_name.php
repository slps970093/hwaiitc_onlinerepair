<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class device_name extends Model
{
    //
    protected $table = 'device_name';
    protected $fillable = [
        'extends','name'
    ];
    public $timestamps = false;
    public function category(){
        return $this->belongsTo(device_category::class,'extends');
    }
}
