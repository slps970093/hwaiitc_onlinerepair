<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_category', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });
        Schema::create('device_name', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('extends')->comment('繼承設備分類');
            $table->foreign('extends')->on('device_category')->references('id')->onDelete('cascade')->comment('繼承設備分類');
            $table->string('name');
        });
        Schema::create('status', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });
        Schema::create('school_year', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->boolean('isEnable')->default(0);
            $table->date('start_date');
            $table->date('end_date');
            $table->timestamps();
        });
        Schema::create('fault_category', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('extends')->comment("繼承設備名稱");
            $table->foreign('extends')->references('id')->on('device_name')->onDelete('cascade');
            $table->string('name');
        });
        Schema::create('device_repair', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('school_year_id')->index()->comment('學年度ID');
            $table->unsignedInteger('device_category_id')->index()->comment('設備分類編號');
            $table->unsignedInteger('device_name_id')->index()->comment('設備名稱編號');
            $table->unsignedInteger('fault_category_id')->index()->comment('故障分類編號');
            $table->foreign('school_year_id')->references('id')->on('school_year')->onDelete('cascade');
            $table->foreign('device_category_id')->references('id')->on('device_category')->onDelete('cascade');
            $table->foreign('device_name_id')->references('id')->on('device_name')->onDelete('cascade');
            $table->foreign('fault_category_id')->references('id')->on('fault_category')->onDelete('cascade');
            $table->string('location')->comment('位置');
            $table->text('content')->nullable()->comment('內容');
            $table->unsignedInteger('repair_status_id')->comment('狀態編號');
            $table->foreign('repair_status_id')->references('id')->on('status')->onDelete('cascade');
            $table->text('description')->nullable()->comment('敘述');
            $table->timestamps();
        });
        Schema::create('qrcode', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('device_category_id')->index()->comment('設備分類編號');
            $table->unsignedInteger('device_name_id')->index()->comment('設備名稱編號');
            $table->foreign('device_category_id')->references('id')->on('device_category')->onDelete('cascade')->comment('設備分類編號');
            $table->foreign('device_name_id')->references('id')->on('device_name')->onDelete('cascade')->comment('設備名稱編號');
            $table->string('location');
            $table->string('repair_url');
            $table->string('repair_search_url');
            $table->string('repair_filename');
            $table->string('repair_search_filename');
        });
        Schema::create('property_information', function (Blueprint $table) {
            $table->increments('id');
            $table->string('serial')->comment('財產序號');
            $table->integer('start')->comment('起');
            $table->integer('until')->comment('迄');
            $table->string('old_serial')->comment('舊財產序號')->nullable();
            $table->unsignedInteger('device_category_id')->comment('設備分類編號');
            $table->unsignedInteger('device_name_id')->comment('設備名稱編號');
            $table->foreign('device_category_id')->references('id')->on('device_category')->onDelete('cascade')->comment('設備分類編號');
            $table->foreign('device_name_id')->references('id')->on('device_name')->onDelete('cascade')->comment('設備名稱編號');
            $table->string('brand')->comment('廠牌');
            $table->string('model')->comment('型號');
            $table->string('specification')->nullable()->comment('規格');
            $table->integer('qty')->comment('數量');
            $table->char('unit',10)->comment('單位');
            $table->string('original_location')->comment('原始地點');
            $table->string('current_location')->commect('目前地點');
            $table->integer('year')->commect('年限');
            $table->string('custodian')->commect('保管人');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device_category');
        Schema::dropIfExists('device_name');
        Schema::dropIfExists('status');
        Schema::dropIfExists('school_year');
        Schema::dropIfExists('fault_category');
        Schema::dropIfExists('device_repair');
        Schema::dropIfExists('qrcode');
        Schema::dropIfExists('property_information');
    }
}
