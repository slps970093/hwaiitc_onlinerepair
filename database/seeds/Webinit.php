<?php

use Illuminate\Database\Seeder;
use App\Admin;
use App\webinfo;
use App\RepairStatus;
class Webinit extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Web info init
        webinfo::create([
           'title' => '線上報修系統',
           'keyword' => null,
           'description' => null
        ]);
        //Admin data init
        Admin::create([
            'name' => 'System Admin',
            'username' => 'admin',
            'password' => Hash::make('secret')
        ]);
        // Repair Status
        RepairStatus::create([
            'name' => '已通報'
        ]);
    }
}
